import cv2
import time

if __name__ == '__main__':

    # Start default camera
    video = cv2.VideoCapture(0);

    # Find OpenCV version
    (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')

    # With webcam get(CV_CAP_PROP_FPS) does not work.
    # Let's see for ourselves.
    video.set(cv2.CAP_PROP_FPS, 30)
    if int(major_ver) < 3:
        fps = video.get(cv2.cv.CV_CAP_PROP_FPS)
        print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS) 3: {0}".format(fps))

    else:
        fps = video.get(cv2.CAP_PROP_FPS)
        print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))


    print("Setting Resolution")
    video.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    video.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
    video.set(cv2.CAP_PROP_FPS, 110)

    # Number of frames to capture
    num_frames = 300;

    print("Capturing {0} frames".format(num_frames))

    # Start time
    start = time.time()

    # Grab a few frames
    for i in range( num_frames):
        ret, frame = video.read()
        cv2.imwrite("tempData/{0}.jpg".format(i),frame)
    # End time
    end = time.time()

    # Time elapsed
    seconds = end - start
    print("Time taken : {0} seconds".format(seconds))


    # Calculate frames per second
    fps = num_frames / seconds;
    print("Estimated frames per second : {0}".format(fps))


    # Release video
    video.release()