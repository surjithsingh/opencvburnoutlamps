
from enum import Enum
from scipy.spatial import distance

class states(Enum):
    INIT = 0
    PREVIOUS_TRANSITION = 1
    PREVIOUS_STABLE = 2
    NEXT_TRANSITION = 3
    NEXT_STABLE = 4


class State(object):
    """
    We define a state object which provides some utility functions for the
    individual states within the state machine.
    """

    def __init__(self,prev, next, slope_change_indexes,transition_time):
        self.prev = prev
        self.next = next
        self.slope_changes = slope_change_indexes
        if len(slope_change_indexes) < 1:

        self.state = self.INIT

    def on_call(self,dist,slope,current,x,current_pointer):
        if self.state == self.INIT:
            dist_prev = distance.euclidean(current, self.prev)
            dist_next = distance.euclidean(current, self.next)
            if(dist_prev < dist_next):
                if slope == 0:
                    self.state = states.PREVIOUS_STABLE
                if slope != 0:
                    self.state = states.PREVIOUS_TRANSITION
            else:

                # slope check

                slope

            if value

    def initial(self):
        self.next


    def on_event(self, event):



