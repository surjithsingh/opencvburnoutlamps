
#Statemachines to know the state of the lamp

'''
States
1. Before transition
2. After Transition
3. Final state reached
4. Next state Started

- Previous State
- Next State
- Known states is a dictionary

- Have to handle a condition where it is vertical line, i.e x is constant and y increases
'''

from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import distance
from enum import Enum



class state(Enum):
    INIT = 0
    PREVIOUS_TRANSITION = 1
    PREVIOUS_STABLE = 2
    NEXT_TRANSITION = 3
    NEXT_STABLE = 4





class stateOfLamp:
    def __init__(self, known_states, window = 5):

        #self.previous_state = previous_state
        #self.next_state = next_state
        self.known_states = known_states
        self.window = window

        self.current_state = state.INIT

    def detectState(self,x,y,v,prev_state,next_state):
        self.x = x
        self.y = y
        self.size = len(x)
        print("Size ",self.size,x[0:10])
        state_prev = False
        state_current = False
        f, (ax1, ax2) = plt.subplots(1, 2, sharex=True)
        f1, (ax3, ax4) = plt.subplots(1, 2, sharex=True)
        ax1.plot(x)
        ax1.set_ylim(0.2,0.8)
        ax2.plot(y)
        ax2.set_ylim(0.2, 0.8)
        ax3.plot(v)
        ax3.set_ylim(0,255)

        slope_x_array = []
        slope_y_array = []



        for start in range(0,self.size,self.window):
            slope_x, intercept_x, r_value, p_value, std_err = stats.linregress([i for i in range(len(x[start:start+self.window]))],x[start:start+self.window])
            slope_y, intercept_y, r_value, p_value, std_err = stats.linregress([i for i in range(len(y[start:start + self.window]))],y[start:start + self.window])

            slope_x_array.append(slope_x)
            slope_y_array.append(slope_y)

            max_window = len(x[start:start+self.window])

            #check stable state
            if slope_x == 0:
                print("X stable")
                # check for previous state or next state
                dist_prev = distance.euclidean(x[start + max_window - 1], self.known_states[next_state][0])
                dist_next = distance.euclidean(x[start + max_window - 1], self.known_states[prev_state][0])
                if dist_prev < dist_next:
                    self.current_state = state.PREVIOUS_STABLE
                    print("Transition Just Started - Distance Close to Previous state")
                else:
                    self.current_state = state.NEXT_STABLE
                    print("Transition Completed - Distance Close to Next state")
            else:
                expected_slope_x, intercept_x, r_value, p_value, std_err = stats.linregress([1, 2], [x[start], self.known_states[next_state][0]])

                if expected_slope_x > 0:
                    if slope_x <= 0:
                        dist = distance.euclidean(x[start + max_window - 1], self.known_states[next_state][0])
                        if dist > 0.05:
                            print("Problem X")

                if expected_slope_x < 0:
                    if slope_x >= 0:
                        dist = distance.euclidean(x[start + max_window - 1], self.known_states[next_state][0])
                        if dist > 0.05:
                            print("Problem X")

            if slope_y == 0:
                print("Y Stable")
                dist_prev = distance.euclidean(y[start + max_window - 1], self.known_states[next_state][1])
                dist_next = distance.euclidean(y[start + max_window - 1], self.known_states[prev_state][1])
                if dist_prev < dist_next:
                    print("Transition Just Started - Distance Close to Previous state")
                else:
                    print("Transition Completed - Distance Close to Next state")
                # check for previous state or next state
            else:
                expected_slope_y, intercept_x, r_value, p_value, std_err = stats.linregress([1,2], [y[start],self.known_states[next_state][1]])
                if expected_slope_y > 0:
                    if slope_y <= 0:
                        dist = distance.euclidean(y[start + max_window - 1], self.known_states[next_state][1])
                        if dist > 0.05:
                            print("Problem Y")

                if expected_slope_y < 0:
                    if slope_y >= 0:
                        dist = distance.euclidean(y[start + max_window - 1], self.known_states[next_state][1])
                        if dist > 0.05:
                            print("Problem Y")

            #print("Expected ",expected_slope_x,expected_slope_y)
            print("Actual " ,slope_x,slope_y)


            #if (len(x[start:start+self.window]) == self.window):
            ax1.plot([i for i in range(start, start+max_window)],slope_x*np.array([i for i in range(len(x[start:start+max_window]))]) + intercept_x)
            ax2.plot([i for i in range(start, start+max_window)],slope_y*np.array([i for i in range(len(y[start:start+max_window]))]) + intercept_y)

                #ax3.plot([i for i in range(start, start + self.window)],
                #        expected_slope_x * np.array([i for i in range(len(x[start:start + self.window]))]) + intercept_x)
                #ax4.plot([i for i in range(start, start + self.window)],
                #        expected_slope_y * np.array([i for i in range(len(y[start:start + self.window]))]) + intercept_y)

        plt.show()

