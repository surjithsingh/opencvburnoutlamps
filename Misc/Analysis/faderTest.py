import numpy as np
import os
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import uuid
import math
from dateutil import tz
import datetime
import peakutils
from stateMachine import stateOfLamp

NoCal = True
transitions = 1
#path = '../../indData/sceneData/'
path = '../../indData/timeSyncData/'
sessionCount = len(os.listdir(path))
FileCount = 24
LampCount = 1
totalCount = FileCount*LampCount
Columns = 4
Grid = math.ceil(totalCount/Columns)

print(sessionCount)
from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/Austin')

utc = datetime.datetime.strptime("2018-01-15 22:05:41.126","%Y-%m-%d %H:%M:%S.%f")
utc = utc.replace(tzinfo=from_zone)
central = utc.astimezone(to_zone)


def analysis(x,y,axXY,axdXY,ind,Columns,file_x,file_y):
    dx = np.diff(x)
    dy = np.diff(y)
    d2x = np.diff(dx)
    d2y = np.diff(dy)
    change_x = np.diff(d2x)
    change_y = np.diff(d2y)

    dx = dx*dx*1000
    dy = dy*dy*1000
    if max(dx) !=0:
        thresh_x = 0.0015 / max(dx)
        thresh_y = 0.0015 / max(dy)
    else:
        thresh_x = 0.0015
        thresh_y = 0.0015

    #dont check peak
    if (dx == 0).all():
        idx = []
    else:
        idx = peakutils.indexes(dx, thres=thresh_x, min_dist=20)
    if (dy == 0).all():
        idy = []
    else:
        idy = peakutils.indexes(dy, thres=thresh_y, min_dist=20)

    #idx = dx.argsort()[-3:][::-1]
    #idy = dy.argsort()[-3:][::-1]
    filteredIndex = []

    print("points ", dx[idx], dy[idy])
    for p in idx:
        if dx[p] < 0.05:
            continue
        startIndex = 0
        for slope in dx[:p][::-1]:
            if slope < 0.05:
                break
            else:
                startIndex +=1
        p = p- startIndex
        filteredIndex.append(p)

    for p in idy:
        if dy[p] < 0.05:
            continue
        startIndex = 0
        for slope in dy[:p][::-1]:
            if slope < 0.05:
                break
            else:
                startIndex +=1
        p = p- startIndex
        filteredIndex.append(p)

    filteredIndex = list(set(filteredIndex))


    validIndex = [0]
    for p in filteredIndex:
        valid = None
        for i in filteredIndex:
            if i == p:
                valid = True
                break
            elif abs(i-p) < 10:
                valid = False
                break
        if not valid:
            continue

        validIndex.append(p)

        axXY[ind // Columns][ind % Columns].plot(X[p], Y[p], "co", label=p)
        #axdXY[ind // Columns][ind % Columns].plot(p, dx[p], "co", label=p)

    print(validIndex)

    color = ["r--",'g--','b--',"c--"]

    for i in range(len(validIndex)):
        print("Valid ",i )
        start = validIndex[i]
        if i == len(validIndex) -1:
            end = -1
            line = Line2D([x[validIndex[i]], file_x], [y[validIndex[i]], file_y])
            line.set_color("c")
            line.set_marker("d")
            axdXY[ind // Columns][ind % Columns].add_line(line)
            axdXY[ind // Columns][ind % Columns].plot(file_x, file_y, "ro")
        else:
            end = validIndex[i+1]



        axdXY[ind // Columns][ind % Columns].plot(x[start:end], y[start:end], color[i], label=i)

        axdXY[ind // Columns][ind % Columns].set_ylim([0.20, 0.80])
        axdXY[ind // Columns][ind % Columns].set_xlim([0.20, 0.80])
    #axdXY[ind // Columns][ind % Columns].plot(dx)
    #axdXY[ind // Columns][ind % Columns].plot(dy)
    return validIndex


def state(x,y,v, prev_state,next_state):
    known_states = {0: [0.4369, 0.404, 100], 1: [0.640, 0.330, 10], 2: [0.27, 0.27,  50], 3: [0.280, 0.288, 100]}
    state_class = stateOfLamp(known_states)
    print(x.shape)
    state_class.detectState(x,y,v,prev_state,next_state)
    return 0



def ratio(xy_1,xy_0,time):
    pass


#calculate slope changing point


def Slope(p1,p2):
    print(p1,p2)
    if (p2[0]-p1[0]) == 0:
        m = (p2[1] - p1[1])
    else:
        m = (p2[1] - p1[1]) / (p2[0] - p1[0])


    if m > 0:
        return True
    else:
        return False






time = {"0":5,"1":2,"2":0,3:0}
scenes = {"0": [0.4369,0.404],"1": [0.640,0.330],"2": [0.27,0.27],"3": [0.280,0.288]}











#PlotSessions = {"f330f5da-41c5-44cb-bb67-85d6ce09fa96": True,"3c29fad4-93af-4289-a22d-2e37cfe68e88":True,"a0390bf9-ebfb-4882-b407-0f6d9b02aab4":True}
#PlotSessions = {"eb1ba32f-15c4-4af4-b68d-b688b0d46ee1": True}

PlotSessions = {"4880097c-9d37-44fe-a659-b5cc812d96d8": True,"eb1ba32f-15c4-4af4-b68d-b688b0d46ee1": True,"c68f4f9e-eb2d-43ce-9939-ebd59f38a81b": True,"de52b038-4dc2-4ad3-b826-8b750532ade9":True}
plotAll = False
xy = True

#fig, ax = plt.subplots(nrows=int(transitions), ncols=int(1), squeeze=False, sharey=True)

H_ = [None] * transitions
for k in range(transitions):
    H_[k] = [None] * sessionCount
    #j = 0
    for session in os.listdir(path):
        print(session)
        if not plotAll:
            try:
                if PlotSessions[session]:
                    pass
            except:
                continue
        j = 0
        z = 0
        validSession = True
        try:
            val = uuid.UUID(session, version=4)
        except ValueError:
            # If it's a value error, then the string
            # is not a valid hex code for a UUID.
            validSession = False
            continue
        print(session)
        currPath = path + str(session)+'/'
        X = [0] * LampCount
        Y = [0] * LampCount
        H = [0] * LampCount
        V = [0] * LampCount
        S = [0] * LampCount

        figXY, axXY = plt.subplots(nrows=int(Grid), ncols=int(Columns), squeeze=False, sharey=True)
        figXY.suptitle(session, fontsize=16)

        figdXY, axdXY = plt.subplots(nrows=int(Grid), ncols=int(Columns), squeeze=False, sharey=True)
        figdXY.suptitle(session, fontsize=16)

        figBright,axBright = plt.subplots(nrows=int(Grid), ncols=int(Columns), squeeze=False, sharey=True)
        figBright.suptitle(session+" Brightness", fontsize=16)

        prev_x = 0
        prev_y = 0
        X_calAdjustment = None
        Y_calAdjustment = None

        csdf = pd.read_csv(
            "C:/Users/Surjith/PycharmProjects/opencvburnoutlamps/indData/timeSyncData/"+session+"/cs.csv")
        camdf = pd.read_csv(
            "C:/Users/Surjith/PycharmProjects/opencvburnoutlamps/indData/timeSyncData/"+session+"/fadeData/lookup.csv")

        for i in os.listdir(currPath)[ 1:]:
            if(j > FileCount):
                continue
            print(i)
            if xy:
                if "xy_" in i:
                    a = np.load(currPath + i)
                    hsv = np.load(currPath+"hsv_"+i[3:])

                    for l in range(a.shape[0]):
                        x = [x_[0] for x_ in a[l]]
                        y = [y_[1] for y_ in a[l]]
                        h = [h_[0] for h_ in hsv[l]]
                        s = [h_[1] for h_ in hsv[l]]
                        v = [h_[2] for h_ in hsv[l]]
                        prev = None
                        next = None
                        #csdf["Timestamp"] = pd.to_datetime(csdf["Timestamp"])
                        prev_x = 0
                        prev_y = 0
                        prev_content = 0
                        camdf["Timestamp"] = pd.to_datetime(camdf["Timestamp"])
                        for ind, data in csdf.iterrows():
                            utc = datetime.datetime.strptime(str(data["Timestamp"]), "%Y-%m-%d %H:%M:%S.%f")
                            utc = utc.replace(tzinfo=from_zone)
                            central = utc.astimezone(to_zone)
                            central = central.replace(tzinfo=None)
                            print(utc,central)
                            print(data["content"])
                            if ind == 0:
                                prev = central
                                continue
                            else:

                                file_x, file_y = scenes[str(data["content"])]
                                filtered = camdf[ camdf.Timestamp > prev + datetime.timedelta(0, 0,200000)  ]
                                print(filtered.shape, central , prev)
                                filtered = filtered[filtered.Timestamp < central+datetime.timedelta(0, 0,300000) ]
                                if filtered.shape[0] == 0:
                                    continue
                                print(filtered.shape, central + datetime.timedelta(0, 1), prev)
                                start = filtered.ind.iloc[0]
                                end = filtered.ind.iloc[-1]
                                ind = ind
                                print("Index",ind,start,end)
                                line = None
                                if ind == 1:
                                    print("Calibrating ",x[start+1],y[start+1])
                                    X_calAdjustment = scenes["0"][0] - x[start+2]
                                    Y_calAdjustment = scenes["0"][1] - y[start+2]
                                    print("Cal Values: ", X_calAdjustment, Y_calAdjustment)
                                    prev_x,prev_y = scenes["0"]
                                    if NoCal:
                                        X_calAdjustment = 0
                                        Y_calAdjustment = 0

                                line = Line2D([prev_x, file_x], [prev_y, file_y])
                                line.set_color("r")
                                ind = ind -1
                                print("Cal Values: ", X_calAdjustment, Y_calAdjustment, [prev_x, file_x], [prev_y, file_y],line)
                                #X = np.array(x[start:end]) + X_calAdjustment
                                #Y = np.array(y[start:end]) + Y_calAdjustment
                                X = np.array([x[subsample] for subsample in range(start,end,3) ])
                                Y = np.array([y[subsample] for subsample in range(start, end,3)])
                                V = np.array([v[subsample] for subsample in range(start, end, 3)])
                                analysis(X,Y,axXY,axdXY,ind,Columns,file_x,file_y)
                                state(X,Y,V,prev_content,data["content"])
                                '''
                                dx = np.diff(X)
                                dy = np.diff(Y)
                                d2x = np.diff(dx)
                                d2y = np.diff(dy)
                                print("D2X, ",d2x)
                                print("D2Y, ", d2y)
                                change_x = np.diff(d2x)
                                change_y = np.diff(d2y)


                                dy_dx = dy/dx
                                dy_dx = np.nan_to_num(dy_dx)
                                d2y_dx2 = np.diff(dy)/np.diff(dx)
                                d2y_dx2 = np.nan_to_num(d2y_dx2)
                                change = np.diff(d2y_dx2)

                                print("Velocity",dy_dx)
                                print("Acceleration",d2y_dx2)

                                #idx = d2y_dx2.nonzero()
                                change = np.nan_to_num(change)
                                #change_x[(change_x < 0.01) & (change_x > -0.01)] = 0
                                #change_y[(change_y < 0.01) & (change_y > -0.01)] = 0
                                print("D3X, ", change_x)
                                print("D3Y, ", change_y)

                                idx = change_x.argsort()[-3:][::-1]
                                idy = change_y.argsort()[-3:][::-1]
                                for p in idx:
                                    axXY[ind // Columns][ind % Columns].plot(X[p],Y[p],"co",label = p)

                                for p in idy:
                                    axXY[ind // Columns][ind % Columns].plot(X[p],Y[p],"mo",label = p)


                                axdXY[ind // Columns][ind % Columns].plot(change_y)
                                axdXY[ind // Columns][ind % Columns].plot(change_x)

                                '''

                                axBright[ind // Columns][ind % Columns].plot(V,label=len(V))
                                axXY[ind // Columns][ind % Columns].plot(X, Y, "bx", label=len(X))
                                axXY[ind // Columns][ind % Columns].plot(x[start+1:start+2], y[start+1:start+2], 'ro', x[end], y[end], 'go')
                                axXY[ind // Columns][ind % Columns].plot(0.43, 0.40,'1', 0.64,0.33,"2",0.15,0.076,"3",0.28,0.28,"4")
                                axXY[ind // Columns][ind % Columns].set_ylim([0.20, 0.80])
                                axXY[ind // Columns][ind % Columns].set_xlim([0.20, 0.80])
                                #axXY[ind // Columns][ind % Columns].add_line(line)
                                axXY[ind // Columns][ind % Columns].set_title(data["content"])
                                axXY[ind // Columns][ind % Columns].legend()
                                prev_x = x[end]
                                prev_y = y[end]
                            prev = central
                            prev_content = data["content"]


    plt.show()