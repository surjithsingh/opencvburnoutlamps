import numpy as np
import os
from mpl_toolkits.mplot3d import Axes3D

from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import uuid
import math


def Slope(p1,p2):
    print(p1,p2)
    if (p2[0]-p1[0]) == 0:
        m = (p2[1] - p1[1])
    else:
        m = (p2[1] - p1[1]) / (p2[0] - p1[0])


    if m > 0:
        return True
    else:
        return False



transitions = 1
#path = '../../indData/sceneData/'
path = '../../indData/timeSyncData/'
sessionCount = len(os.listdir(path))
FileCount = 17
LampCount = 1
totalCount = FileCount*LampCount
Columns = 4
Grid = math.ceil(totalCount/Columns)

#PlotSessions = {"f330f5da-41c5-44cb-bb67-85d6ce09fa96": True,"3c29fad4-93af-4289-a22d-2e37cfe68e88":True,"a0390bf9-ebfb-4882-b407-0f6d9b02aab4":True}
PlotSessions = {"c1b4201f-b83b-4051-a980-25a944112303": True, "4880097c-9d37-44fe-a659-b5cc812d96d8":True}
plotAll = False
xy = True

#fig, ax = plt.subplots(nrows=int(transitions), ncols=int(1), squeeze=False, sharey=True)

H_ = [None] * transitions
for k in range(transitions):
    H_[k] = [None] * sessionCount
    #j = 0
    for session in os.listdir(path):
        if not plotAll:
            try:
                if PlotSessions[session]:
                    pass
            except:
                continue
        j = 0
        z = 0
        validSession = True
        try:
            val = uuid.UUID(session, version=4)
        except ValueError:
            # If it's a value error, then the string
            # is not a valid hex code for a UUID.
            validSession = False
            continue
        print(session)
        currPath = path + str(session)+'/'
        X = [0] * LampCount
        Y = [0] * LampCount
        H = [0] * LampCount
        V = [0] * LampCount
        S = [0] * LampCount

        figPXY, axPXY = plt.subplots(nrows=int(Grid), ncols=int(Columns), squeeze=False, sharey=True)
        figXY, axXY = plt.subplots(nrows=int(Grid), ncols=int(Columns), squeeze=False, sharey=True)
        figHV, axHV = plt.subplots(nrows=int(Grid), ncols=int(Columns), squeeze=False, sharey=True)
        figHV, axS = plt.subplots(nrows=int(Grid), ncols=int(Columns), squeeze=False, sharey=True)
        #figXY = plt.figure()
        #axXY = figXY.add_subplot(111, projection='3d')
        figXY.suptitle(session, fontsize=16)
        prev_x = 0
        prev_y = 0

        for i in os.listdir(currPath)[ 1:]:
            if(j > FileCount):
                continue
            print(i)
            if xy:
                if "xy_" in i:
                    a = np.load(currPath + i)
                    nameSplit = i.split('_')
                    file_x = float(nameSplit[2])
                    file_y = float(nameSplit[3])




                    for l in range(a.shape[0]):
                        x = [x_[0] for x_ in a[l]]
                        y = [y_[1] for y_ in a[l]]
                        X[l] = np.append(X[l], np.array(x))
                        Y[l] = np.append(Y[l], np.array(y))
                        posSlope = Slope([prev_x, prev_y], [file_x, file_y])
                        line = Line2D([prev_x, file_x], [prev_y, file_y])
                        '''
                        if posSlope:
                            line = Line2D([prev_x, file_x], [prev_y, file_y])
                        else:
                            line = Line2D([file_x,prev_x], [file_y,prev_y])
                        '''
                        ind = l*FileCount + j

                        axPXY[ind // Columns][ind % Columns].add_line(line)

                        axPXY[ind // Columns][ind % Columns].set_ylim([0.0, 1])
                        axPXY[ind // Columns][ind % Columns].set_xlim([0, 1])
                        axPXY[ind // Columns][ind % Columns].legend()
                        axXY[ind//Columns][ind%Columns].plot(x[1:],y[1:],'bx',x[1:],y[1:],label = len(x))
                        title = "{0}-{1}-{2}".format(nameSplit[2], nameSplit[3], nameSplit[5])
                        axXY[ind//Columns][ind%Columns].plot(x[1:2],y[1:2],'ro',x[-1],y[-1],'go')
                        axXY[ind//Columns][ind%Columns].set_title(title)
                        axXY[ind//Columns][ind%Columns].set_ylim([0.20, 0.80])
                        axXY[ind // Columns][ind % Columns].set_xlim([0.20, 0.80])
                        axXY[ind//Columns][ind%Columns].legend()

                    #plt.show()
                    prev_x = file_x
                    prev_y = file_y

                    j += 1
                elif "hsv_" in i:
                    a = np.load(currPath + i)
                    nameSplit = i.split('_')

                    for l in range(a.shape[0]):
                        h = [h_[0] for h_ in a[l]]
                        v = [v_[2] for v_ in a[l]]
                        s = [v_[1] for v_ in a[l]]

                        H[l] = np.append(H[l], np.array(h))
                        V[l] = np.append(V[l], np.array(v))
                        S[l] = np.append(S[l], np.array(s))

                        ind = l*FileCount + z

                        axHV[ind//Columns][ind%Columns].plot(range(len(h[1:])),h[1:],'bx',range(len(h[1:])),h[1:],label = len(h))
                        title = "{0}-{1}-{2}".format(nameSplit[2], nameSplit[3], nameSplit[5])
                        axHV[ind//Columns][ind%Columns].set_title(title)
                        axHV[ind//Columns][ind%Columns].set_ylim([0, 255])
                        axHV[ind//Columns][ind%Columns].legend()

                        axS[ind // Columns][ind % Columns].plot(range(len(s[1:])), s[1:], 'bx', range(len(s[1:])), s[1:],
                                                                 label=len(h))
                        title = "{0}-{1}-{2} - Saturation".format(nameSplit[2], nameSplit[3], nameSplit[5])
                        axS[ind // Columns][ind % Columns].set_title(title)
                        axS[ind // Columns][ind % Columns].set_ylim([0, 255])
                        axS[ind // Columns][ind % Columns].legend()

                    #plt.show()
                    z += 1

            else:
                a = np.load(currPath+i)
                a = np.mod(a, 158)
                h = [h_[0] for h_ in a[0]]
                H_[k][j] = h
                # print(input())
                ax[k][0].plot(h, label=str(j))
                ax[k][0].legend()
            #j += 1

        '''
        for l in range(4):
            index = np.array([z for z in range(X[l].shape[0])])
            print(X[l].shape,Y[l].shape)
            axXY.plot(index,X[l], Y[l])
            axXY.legend()
        plt.show()
        '''
'''
H_ = np.array(H_)

sampleLength = [None]*transitions
for i in range(transitions):
    ar = None
    for j in range(sessionCount):
        if H_[i][j] == None:
            continue
        if j == 0:
            ar = [len(H_[i][j])]
            print(ar)
        else:
            ar.append(len(H_[i][j]))
    sampleLength[i] = np.min(ar)
    print(sampleLength[i])



fig1, ax1 = plt.subplots(nrows=int(1), ncols=int(transitions), squeeze=False, sharey=True)

avgData = [None]*transitions

for i in range(transitions):
    sessionArray = None
    for j in range(sessionCount):
        if H_[i][j] == None:
            continue
        if j == 0:
            sessionArray = np.array([H_[i][j][:sampleLength[i]]])
            print(len(H_[i][j][:sampleLength[i]]))
            #print(sessionArray)
        else:
            print(len(H_[i][j][:sampleLength[i]]))
            sessionArray = np.append(sessionArray,np.array([H_[i][j][:sampleLength[i]]]),axis = 0)
    #print(sessionArray.shape)
    avgData[i] = np.average(sessionArray,axis = 0)
    avgData[i] = np.array(avgData[i])
    #print(avgData[i])
    ax1[0][i].plot(avgData[i],label=str(j))
    ax1[0][i].legend()


Score = np.ndarray((transitions,sessionCount))

for i in range(transitions):
    sessionArray = None
    for j in range(sessionCount):
        if H_[i][j] == None:
            continue
        if len(avgData[i]) < len(H_[i][j]):
            g_ = avgData[i]
            h_ = H_[i][j][:avgData[i].shape[0]]
        elif len(avgData[i]) > len(H_[i][j]):
            g_ = avgData[i][:H_[i][j].shape[0]]
            h_ = H_[i][j]
        else:
            g_ = avgData[i]
            h_ = H_[i][j]

        mat = np.corrcoef(g_, h_)
        Score[i][j] = mat[0][1]
        if Score[i][j] <= 0.90:
            print(Score[i][j],i,j)


print("Minimum")
print(np.nanmin(Score))

'''
plt.show()
