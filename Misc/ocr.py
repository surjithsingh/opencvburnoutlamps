from PIL import Image
import pytesseract
import cv2
import os
import zbar

scale = 6
delta = 0
ddepth = cv2.CV_16S
scanner = zbar.ImageScanner()
scanner.parse_config('enable')
# cam = cv2.VideoCapture(0)

for i in range(1):
    image = cv2.imread('stitched.jpg')
    image = cv2.GaussianBlur(image, (3, 3), 0)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    '''
    # Gradient-X
    grad_x = cv2.Sobel(gray,ddepth,1,0,ksize = 3, scale = scale, delta = delta,borderType = cv2.BORDER_DEFAULT)
    #grad_x = cv2.Scharr(gray,ddepth,1,0)

    # Gradient-Y
    grad_y = cv2.Sobel(gray,ddepth,0,1,ksize = 3, scale = scale, delta = delta, borderType = cv2.BORDER_DEFAULT)
    #grad_y = cv2.Scharr(gray,ddepth,0,1)

    abs_grad_x = cv2.convertScaleAbs(grad_x)   # converting back to uint8
    abs_grad_y = cv2.convertScaleAbs(grad_y)

    dst = cv2.addWeighted(abs_grad_x,0.5,abs_grad_y,0.5,0)
    '''
    print(gray)

    gray = cv2.threshold(gray, 110, 255,
                         cv2.THRESH_BINARY)[1]
    # gray = cv2.medianBlur(gray, 3)
    filename = "{}.png".format(os.getpid())
    cv2.imwrite(filename, gray)
    pil = Image.fromarray(gray)
    width, height = pil.size
    raw = pil.tobytes()
    image = zbar.Image(width, height, 'Y800', raw)
    scanner.scan(image)
    for symbol in image:
        print
        'decoded', symbol.type, 'symbol', '"%s"' % symbol.data

    text = pytesseract.image_to_string(Image.open(filename))
    os.remove(filename)
    print(text)

    # show the output images
    # cv2.imshow("Image", cv2.resize(image,(640,480)))
    # cv2.imshow("sobel", dst)
    cv2.imshow("Output", gray)
    cv2.waitKey(0)
