
import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from keras.optimizers import RMSprop
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
from sklearn.model_selection import learning_curve

class dataset:
    def __init__(self,percent=0.8):
        self.rawPD = pd.read_csv("updatedReg.csv")
        self.train = pd.DataFrame()
        self.test = pd.DataFrame()
        self.trainX = None
        self.trainY= None
        self.testX = None
        self.testY = None
        self.dataX = None
        self.dataY = None
        print("Creating a data from CSV")
        self.dataSplit(percent)
        self.getIO()
        print("Created a data from CSV")
        #self.reshape()
        self.normalize()
        print("Normalized inputs")


    def dataSplit(self,percent):
        msk = np.random.rand(self.rawPD.shape[0]) < percent
        self.train = self.rawPD[msk]
        self.test = self.rawPD[~msk]

    def getIO(self):
        camData = pd.DataFrame()
        cmdSent = pd.DataFrame()
        camData['R'] = self.train['B_read']
        camData['G'] = self.train['G_read']
        camData['B'] = self.train['R_read']
        #camData['Brightness'] = self.train['Brightness']/257
        cmdSent['i_R'] = self.train['R']
        cmdSent['i_G'] = self.train['G']
        cmdSent['i_B'] = self.train['B']
        self.trainX = camData.as_matrix()
        self.trainY = cmdSent.as_matrix()
        camData = pd.DataFrame()
        cmdSent = pd.DataFrame()
        camData['R'] = self.test['B_read']
        camData['G'] = self.test['G_read']
        camData['B'] = self.test['R_read']
        #camData['Brightness'] = self.train['Brightness'] / 257
        #cmdSent['X'] = self.test['X_sent']
        #cmdSent['Y'] = self.test['Y_sent']
        cmdSent['i_R'] = self.test['R']
        cmdSent['i_G'] = self.test['G']
        cmdSent['i_B'] = self.test['B']
        self.testX = camData.as_matrix()
        self.testY = cmdSent.as_matrix()
        camData = pd.DataFrame()
        cmdSent = pd.DataFrame()
        camData['R'] = self.rawPD['B_read']
        camData['G'] = self.rawPD['G_read']
        camData['B'] = self.rawPD['R_read']
        #camData['Brightness'] = self.train['Brightness'] / 257
        #cmdSent['X'] = self.rawPD['X_sent']
        #cmdSent['Y'] = self.rawPD['Y_sent']
        cmdSent['i_R'] = self.rawPD['R']
        cmdSent['i_G'] = self.rawPD['G']
        cmdSent['i_B'] = self.rawPD['B']
        self.dataX = camData.as_matrix()
        self.dataY = cmdSent.as_matrix()

    def reshape(self):
        self.trainX = self.trainX.reshape(self.trainX.shape[0],1,3)
        self.testX = self.testX.reshape(self.testX.shape[0], 1, 3)
        self.trainY = self.trainY.reshape(self.trainY.shape[0], 1, 3)
        self.testY = self.testY.reshape(self.testY.shape[0], 1, 3)

    def normalize(self):
        self.trainX = self.trainX.astype('float32')
        self.testX = self.testX.astype('float32')
        self.trainY = self.trainY.astype('float32')
        self.testY = self.testY.astype('float32')
        self.trainX /= 255
        self.testX /= 255
        self.trainY /= 255
        self.testY /= 255

class neuralNet():
    def __init__(self,data):
        self.estimator = None
        self.data = data
        self.seed = 7
        self.estimatorInit()


    def evaluate(self):
        kfold = KFold(n_splits=10, random_state=self.seed)
        results = cross_val_score(self.estimator, self.data.trainX, self.data.trainY, cv=kfold)
        print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))


    #define base model
    def baseline_model(self):
        # create model
        model = Sequential()
        model.add(Dense(6, input_dim=3, activation='relu'))
        #model.add(Dense(10, kernel_initializer='random_uniform', activation='selu'))
        #model.add(Dense(6, kernel_initializer='random_uniform', activation='selu'))
        model.add(Dense(3, kernel_initializer='random_uniform',activation='relu'))
        # Compile model
        rmsprop = RMSprop(lr=0.0001)
        model.compile(loss='mean_squared_error', optimizer=rmsprop)
        return model


    def estimatorInit(self):
        np.random.seed(self.seed)
        self.estimator = KerasRegressor(build_fn = self.baseline_model, nb_epoch = 1000, batch_size = 50)




data = dataset()
print(data.trainX.shape,data.trainY.shape)

net = neuralNet(data)
net.evaluate()
net.estimator.fit(data.trainX,data.trainY)
prediction = net.estimator.predict(data.testX)
print("Shape , " ,data.testX.shape)
print(prediction)
print(data.testY)
expectedR = data.testY[:,:1]
expectedG = data.testY[:,1:2]
expectedB = data.testY[:,2:3]
predictedR = prediction[:,:1]
predictedG = prediction[:,1:2]
predictedB = prediction[:,2:3]

fig,(ax1,ax2,ax3) = plt.subplots(nrows= 1, ncols= 3,sharey=True)
ax1.scatter(expectedR,predictedR,label="R")
ax2.scatter(expectedG,predictedG,label="G")
ax3.scatter(expectedB,predictedB,label="B")
#ax2.scatter(predictedX,predictedY,label="Y")

#plt.scatter(data.testY,prediction)
plt.show()

