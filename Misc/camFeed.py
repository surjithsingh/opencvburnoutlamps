import cv2
import threading
import logging
from multiprocessing import Queue
import numpy as np
import imutils
logging.basicConfig(level=logging.DEBUG,format='(%(threadName)-9s) %(message)s',)
import time
BUF_SIZE = 100
q = Queue(1000)





def camFeed():
    cam = cv2.VideoCapture(0)
    print("Setting Resolution")
    cam.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
    cam.set(cv2.CAP_PROP_FPS, 110)
    count = 0
    while True:
        ret,img = cam.read()
        if ret:
            count+=1

            cv2.imshow("frame", hough(img))

            #cv2.imshow("frame",img)
            cv2.imwrite("pano/%d.jpg"%(count),img)
            #time.sleep(100)
            if cv2.waitKey(200) != -1:
                break

    cam.release()


class ShapeDetector:
    def __init__(self):
        pass

    def detect(self, c):
        # initialize the shape name and approximate the contour
        shape = "unidentified"
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)
        # if the shape is a triangle, it will have 3 vertices
        if len(approx) == 3:
            shape = "triangle"

        # if the shape has 4 vertices, it is either a square or
        # a rectangle
        elif len(approx) == 4:
            # compute the bounding box of the contour and use the
            # bounding box to compute the aspect ratio
            (x, y, w, h) = cv2.boundingRect(approx)
            ar = w / float(h)

            # a square will have an aspect ratio that is approximately
            # equal to one, otherwise, the shape is a rectangle
            shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"

        # if the shape is a pentagon, it will have 5 vertices
        elif len(approx) == 5:
            shape = "pentagon"

        # otherwise, we assume the shape is a circle
        else:
            shape = "circle"

        # return the name of the shape
        return shape


def shapeDetection():
    cam = cv2.VideoCapture(1)
    while True:
        ret, image = cam.read()
        if ret:
            resized = imutils.resize(image, width=300)
            ratio = image.shape[0] / float(resized.shape[0])

            # convert the resized image to grayscale, blur it slightly,
            # and threshold it
            gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
            blurred = cv2.GaussianBlur(gray, (5, 5), 0)
            thresh = cv2.threshold(blurred, 90, 255, cv2.THRESH_BINARY)[1]

            # find contours in the thresholded image and initialize the
            # shape detector
            cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)
            cnts = cnts[0] if imutils.is_cv2() else cnts[1]
            sd = ShapeDetector()
            print(cnts)
            # loop over the contours
            for c in cnts:
                # compute the center of the contour, then detect the name of the
                # shape using only the contour
                M = cv2.moments(c)
                cX = int((M["m10"] / M["m00"]) * ratio)
                cY = int((M["m01"] / M["m00"]) * ratio)
                shape = sd.detect(c)

                # multiply the contour (x, y)-coordinates by the resize ratio,
                # then draw the contours and the name of the shape on the image
                c = c.astype("float")
                c *= ratio
                c = c.astype("int")
                cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
                cv2.putText(image, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
                            0.5, (255, 255, 255), 2)

                # show the output image
                cv2.imshow("Image", image)
                cv2.waitKey(0)

            if cv2.waitKey(200) != -1:
                break

    cam.release()






def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect


def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # return the warped image
    return warped

def diff():
    cam = cv2.VideoCapture(1)
    while True:
        ret, img = cam.read()
        if ret:
            img = cv2.cvtColor(np.uint8(img), cv2.COLOR_BGR2GRAY)
            prev = img.copy()
            while True:
                ret, curr = cam.read()
                current =  cv2.cvtColor(np.uint8(curr), cv2.COLOR_BGR2GRAY)
                gray = cv2.threshold(current, 80, 255,
                                     cv2.THRESH_BINARY)[1]
                prev = current.copy()
                cv2.imshow("Diff",gray)
                cv2.imshow("orig", current)
                print(diff)
                cv2.waitKey(10)


def hough(img):
    r = 320.0 / img.shape[1]
    dim = (320, int(img.shape[0] * r))
    # perform the actual resizing of the image and show it
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    img = cv2.medianBlur(img, 5)
    cimg = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # print(cimg.shape)
    circles = cv2.HoughCircles(cimg, cv2.HOUGH_GRADIENT, 1, 20,
                                        param1=150, param2=20, minRadius=80 , maxRadius=85)
    try:
        for i in circles:
            for circle in i:
                print("Drawing Circle", circle)
                #if circle[2] >62 and circle[2] < 70:
                cv2.circle(img, (circle[0], circle[1]), circle[2], (0, 0, 255), 2)

        return img
    except:
        print("failed")
        return img

camFeed()
#diff()
#shapeDetection()