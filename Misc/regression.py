from sklearn import multioutput,linear_model
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sklearn
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.tri as mtri
from sklearn.ensemble import RandomForestRegressor
from rgbxy import Converter
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import Ridge
from tensorflow.contrib import learn
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error
import pickle

def p(x):
    return int(x/3)
def q(x):
    return int(x%3)


def MultiregFit(data):
    input = data[['Brightness','R_read','G_read','B_read']].copy()
    X = np.array(input.values.tolist())
    X = X.reshape(len(X),4)
    o_R = np.array(data['R'])
    o_R = o_R.reshape(len(o_R),1)
    o_G = np.array(data['G'])
    o_G = o_R.reshape(len(o_G), 1)
    o_B = np.array(data['B'])
    o_B = o_R.reshape(len(o_B), 1)

    output = np.array(data[['R','G','B']].copy())
    #output = output.values.tolist()
    regr = linear_model.LinearRegression()
    regr.fit(X,o_R)
    print(regr.coef_)
    out = regr.predict(X)
    print(out)
    plt.figure(1)
    fig, ax = plt.subplots(nrows=3, ncols=1)
    ax[0].plot(data['R'],out,'ro')
    regr.fit(X, o_G)
    print(regr.coef_)
    out = regr.predict(X)
    ax[1].plot(data['G'], out,'go')
    regr.fit(X, o_B)
    print(regr.coef_)
    out = regr.predict(X)
    ax[2].plot(data['B'], out,'bo')

    plt.show()


def indReg(arg):

    orig_par_data = arg.copy()
    fig, ax = plt.subplots(nrows=13, ncols=3)
    inc = 5000
    x = 0
    #regr = make_pipeline(PolynomialFeatures(2), Ridge())
    regr = linear_model.LinearRegression()
    for i in range(5000, 66000, inc):
        data = par_data
        o_R = np.array(data['R'])
        o_R = o_R.reshape(len(o_R), 1)
        temp = pd.DataFrame()
        temp['B_read'] = data['B_read']
        temp['Brightness'] = data['Brightness']
        i_R = np.array(temp)
        i_R = i_R.reshape(len(i_R), 2)

        o_G = np.array(data['G'])
        o_G = o_G.reshape(len(o_G), 1)
        temp = pd.DataFrame()
        temp['G_read'] = data['G_read']
        temp['Brightness'] = data['Brightness']
        i_G = np.array(temp)
        i_G = i_G.reshape(len(i_G), 2)

        o_B = np.array(data['B'])
        o_B = o_B.reshape(len(o_B), 1)
        temp = pd.DataFrame()
        temp['R_read'] = data['R_read']
        temp['Brightness'] = data['Brightness']
        i_B = np.array(temp)
        i_B = i_B.reshape(len(i_B), 2)

        regr.fit(i_R,o_R)
        pickle.dump(regr, open('Red.model', 'wb'))
        print(regr.coef_, "Red")
        print(mean_squared_error(np.array(data['B_read']),regr.predict(i_R)))
        ax[p(x)][q(x)].plot(data['B_read'],regr.predict(i_R), label="Red %d" % (i))
        ax[p(x)][q(x)].plot(data['B_read'], o_R,'rx', label="actual %d" % (i))
        x += 1
        # plt.subplot(312)
        regr.fit(i_G, o_G)
        pickle.dump(regr, open('Green.model', 'wb'))
        print(regr.coef_, "Green")
        print(mean_squared_error(np.array(data['G_read']), regr.predict(i_G)))
        ax[p(x)][q(x)].plot(data['G_read'],regr.predict(i_G), label="Green %d" % (i))
        ax[p(x)][q(x)].plot(data['G_read'], o_G,'gx', label="actual %d" % (i))
        x += 1
        # ax[][].subplot(313)
        regr.fit(i_B, o_B)
        pickle.dump(regr, open('Blue.model', 'wb'))
        print(regr.coef_, "blue")
        print(mean_squared_error(np.array(data['R_read']), regr.predict(i_B)))
        ax[p(x)][q(x)].plot(data['R_read'],regr.predict(i_B), label="Blue %d" % (i))
        ax[p(x)][q(x)].plot(data['R_read'], o_B,'bx', label="actual %d" % (i))
        x += 1

def varFit(x,y,z):
    df = pd.DataFrame()
    df['X'] = x
    df['Y'] = y
    x_train = df.values.tolist()
    y_train = z.tolist() #output_training
    x_test = input

    poly = PolynomialFeatures(degree=3)
    X_ = poly.fit_transform(x_train)
    #predict_ = poly.fit_transform(x_)

    clf = linear_model.LinearRegression()
    clf.fit(X_,y_train)
    ret = clf.predict(X_)
    # The coefficients
    print('Coefficients: \n', clf.coef_)
    # The mean squared error
    print("Mean squared error: %.2f"
          % mean_squared_error(y_train, ret))
    # Explained variance score: 1 is perfect prediction
    print('Variance score: %.2f' % r2_score(y_train, ret))

    ax2 = plt.subplot(111, projection='3d')
    ax2.scatter(df['X'], df['Y'],z, color='black')
    ax2.plot(df['X'],df['Y'],ret, color='blue', linewidth=3)



    plt.show()
    return ret

#regression


max_depth = 30
fig = plt.figure()
ax = fig.add_subplot(211,projection ='3d')
rows = 13
cols = 3
inc = 5000


par_data = pd.read_csv("../PAR600HR.csv")
par_data["Y_sent"] = par_data["Y_sent"].astype('float64')
par_data["X_sent"] = par_data["X_sent"].astype('float64')
par_data["Brightness"] = par_data["Brightness"].astype('float64')

#input = [par_data['X_sent'],par_data['Y_sent'],par_data['Brightness']]
#output = [par_data['X_read'],par_data['Y_read']]
#input = [[getattr(data, i) for i in ['X_sent','Y_sent','Brightness']] for ind,data in par_data.iterrows()]

converter = Converter()

for ind,data in par_data.iterrows():
    rgb = converter.xy_to_rgb(data['X_sent'],data['Y_sent'])
    rgb = list(rgb)
    par_data.set_value(ind,'R',rgb[0])
    par_data.set_value(ind, 'G', rgb[1])
    par_data.set_value(ind, 'B', rgb[2])

    '''
    rgb_n = converter.xy_to_rgb(data['X_read'], data['Y_read'])
    rgb_n = list(rgb)
    par_data.set_value(ind, 'R_r', rgb[0])
    par_data.set_value(ind, 'G_r', rgb[1])
    par_data.set_value(ind, 'B_r', rgb[2])
    '''
#par_data.to_csv("rgbPAR.csv")

#ax.scatter(par_data['B_read'],par_data['G_read'],par_data['R_read'],label='Sent')
#ax2 = fig.add_subplot(212,projection ='3d')
#ax2.scatter(par_data['R'],par_data['G'],par_data['B'],label='sent')
#ax2.plot_surface(par_data['R'],par_data['G'],par_data['B'],label='sent')


#ax.legend()
plt.show()
indReg(par_data)
orig_par_data = par_data.copy()

fig,ax = plt.subplots(nrows= rows, ncols= cols)

x = 0

for i in range(5000,66000,inc):
    par_data = orig_par_data.loc[orig_par_data['Brightness'] == i]
    R_z = np.polyfit(par_data['R'],par_data['B_read'],2)
    G_z = np.polyfit(par_data['G'],par_data['G_read'],2)
    B_z = np.polyfit(par_data['B'],par_data['R_read'],2)
    print("Red", R_z)
    print("Green", G_z)
    print("Blue", B_z)

    f_R = np.poly1d(R_z)
    f_G = np.poly1d(G_z)
    f_B = np.poly1d(B_z)

    '''
    plt.figure(2)
    plt.subplot(211)
    plt.plot(par_data.index, f_R(par_data['R']), 'bo',par_data.index, par_data['B_read'], 'ro')
    plt.subplot(212)
    plt.plot(par_data.index, par_data['B_read'], 'ro')
    #plt.show()
    print(mean_squared_error(par_data['B_read'],f_R(par_data['R'])))
    print(mean_squared_error(par_data['G_read'],f_R(par_data['G'])))
    print(mean_squared_error(par_data['R_read'],f_R(par_data['B'])))
    '''


    #plt.figure(3)
    #plt.subplot(311)
    #ax[][].plot(f_R(par_data['R']),par_data['B_read'],'ro')
    #ax[][].subplot(312)
    #ax[][].plot(f_G(par_data['G']),par_data['G_read'],'go')
    #ax[][].subplot(313)
    #ax[][].plot(f_B(par_data['B']),par_data['R_read'],'bo')
    #plt.show()

    #plt.figure(4)
    #lt.subplot(311)
    '''
    ax[p(x)][q(x)].plot((par_data['R']),par_data['B_read'],'ro',label = "Red %d"%(i))
    x+=1
    #plt.subplot(312)
    ax[p(x)][q(x)].plot((par_data['G']),par_data['G_read'],'go',label = "Green %d"%(i))
    x+=1
    #ax[][].subplot(313)
    ax[p(x)][q(x)].plot((par_data['B']),par_data['R_read'],'bo',label = "Blue %d"%(i))
    '''

    ax[p(x)][q(x)].plot(f_R(par_data['R']), par_data['B_read'], 'ro', label="Red %d" % (i))
    x += 1
    # plt.subplot(312)
    ax[p(x)][q(x)].plot(f_G(par_data['G']), par_data['G_read'], 'go', label="Green %d" % (i))
    x += 1
    # ax[][].subplot(313)
    ax[p(x)][q(x)].plot(f_B(par_data['B']), par_data['R_read'], 'bo', label="Blue %d" % (i))
    x+=1

    '''
    ax[p(x)][q(x)].plot(par_data['R'], varFit(par_data['B'],par_data['Brightness'],par_data['B_read']), 'ro', label="Red %d" % (i))
    x += 1
    # plt.subplot(312)
    ax[p(x)][q(x)].plot(par_data['G'], varFit(par_data['G'],par_data['Brightness'],par_data['G_read']), 'go', label="Green %d" % (i))
    x += 1
    # ax[][].subplot(313)
    ax[p(x)][q(x)].plot(par_data['B'], varFit(par_data['R'],par_data['Brightness'],par_data['R_read']), 'bo', label="Blue %d" % (i))
    x+=1
    '''
for i in range(rows):
    for j in range(cols):
        ax[i][j].legend()
        ax[i][j].set_ylim([0, 255])
        ax[i][j].set_xlim([0, 255])

plt.show()


#clf = linear_model.LinearRegression()
#out = clf.fit(input,par_data['X_read'])
#print(clf.coef_)
'''
#print(np.asarray(input).shape)
#print(np.transpose(np.matrix(input)).shape)
##print(np.transpose(np.matrix(output)).shape)



#clf = linear_model.Ridge()
clf = multioutput.MultiOutputRegressor(sklearn.base.BaseEstimator)
clf.fit(np.transpose(np.matrix(output)),
                     np.transpose(np.matrix(input)))

y = clf.predict(np.transpose(np.matrix(output)))
score = clf.score(np.transpose(np.matrix(output)),y)
print(score)

'''
#print(out.summary)
#predict = (par_data['X_sent']*par_data['X_sent']*clf.coef_[0])+(par_data['X_sent']*clf.coef_[1])+clf.coef_[2]
#ax.plot(par_data['X_sent'],par_data.index)
#x.plot(predict,par_data.index)
#plt.show()
#print(clf.predict(np.transpose(np.matrix(output))))