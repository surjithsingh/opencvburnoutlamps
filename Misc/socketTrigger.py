'''

'import socket
from datetime import datetime

host_ip =socket.gethostbyname(socket.gethostname())

UDP_IP = "10.8.8.87"
UDP_PORT = 14000
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP,UDP_PORT))
while True:
    	data,addr = sock.recvfrom(2048)
    	cli_req_recv = str(datetime.now())
    	print(data.decode("utf-8"),addr)
'''

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

class Echo(DatagramProtocol):

    def datagramReceived(self, data, ip):
        (host, port) = ip
        print("received %r from %s:%d" % (data, host, port))
        data = data.decode("utf-8")
        print(data)
        if "Syncing" in data:
            self.transport.write(bytearray("Synced",'utf8'), (host, port))
        if "XY" in data:
            self.transport.write(bytearray("GotSamples", 'utf8'), (host, port))
        if "Map" in data:
            self.transport.write(bytearray("GotSamples", 'utf8'), (host, port))


reactor.listenUDP(13000, Echo())
reactor.run()