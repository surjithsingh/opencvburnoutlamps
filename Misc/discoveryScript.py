from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
import socket
import json
import time

class Discovery(DatagramProtocol):
    def datagramReceived(self, data, ip):
        (host, port) = ip
        print("received %r from %s:%d" % (data, host, port))
        data = data.decode("utf-8")
        print(data)
        response = {}
        if data.startswith('+'):
            response['ip_address'] = [l for l in (
            [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [
                [(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in
                 [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0]
            response['device_name'] = socket.gethostname()
            json_string = json.dumps(response)
            print("Got Service Announcement from ",host,port)
            print(json_string)
            self.transport.write(bytearray(json_string,'utf8'), (host, port))

reactor.listenUDP(10000, Discovery())
reactor.run()