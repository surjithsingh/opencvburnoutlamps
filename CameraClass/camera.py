import cv2

class camera:
    """
    Camera Class takes the camera index,camera resolution and framerate
    Frame rates are not accurate, but it sets the camera for that value.
    It varies according the IO operation on the computer - Make sure the camera is USB 3.0
    """
    def __init__(self,camIndex = 0 ,img_resolution=(320, 240),cam_resolution=(1600,1200), framerate=32):
        """
        It configures the camera parameters and makes sure that the camera is connected and sending frames
        :param camIndex:
        :param resolution:
        :param framerate:
        """
        # initialize the camera
        self.camera = cv2.VideoCapture(camIndex)
        camSuccess, Frame = self.camera.read()
        while not camSuccess:
            print("Trying to reconnect the camera")
            self.camera.release()
            self.camera = cv2.VideoCapture(camIndex)
            camSuccess, frame = self.camera.read()

        print("Setting Resolution")
        self.camera.set(cv2.CAP_PROP_FRAME_WIDTH, cam_resolution[0])
        self.camera.set(cv2.CAP_PROP_FRAME_HEIGHT, cam_resolution[1])
        self.camera.set(cv2.CAP_PROP_FPS, framerate)
        self.rawCapture = self.camera.read()[1]


        self.resize = img_resolution[0]
        self.sizeFactor = 1.0

        self.frame = None
        self.stopped = False

    def releaseCam(self):
        self.camera.release()

    def resizedFrame(self):
        ret, img = self.camera.read()
        if not ret:
            raise ValueError("Camera not returning image")
        else:
            r = self.resize / img.shape[1]
            dim = (int(self.resize), int(img.shape[0] * r))
            # perform the actual resizing of the image and show it
            self.currentImage = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
            return self.currentImage

    def captureFrame(self):
        ret, img = self.camera.read()
        return img

