from camera import camera
import cv2
import numpy as np
from sklearn import neighbors
from math import sqrt
from rgbxy import Converter
import uuid
import datetime
import pandas as pd
import os


class lampDetection:
    def __init__(self, no_of_lamps,lamp_type,cam_resolution = (640,480)):
        """
        This Class detects the lamps and maps them with its coordinates.
        It takes number of lamps as the arguments and lamp_type
        """
        self.cam = camera(cam_resolution=cam_resolution)
        self.sizeFactor = self.cam.sizeFactor
        self.lamp_count = no_of_lamps
        if lamp_type is "S38":
            self.min_radius = int(43.0/self.sizeFactor)
            self.max_radius = int(50.0/self.sizeFactor)
            self.con_max_radius = int(43.0/self.sizeFactor)
            self.con_min_radius = int(30.0/self.sizeFactor)
            self.param1 = 150
            self.param2 = 20
        elif lamp_type is "PAR":
            self.min_radius = int(20.0/self.sizeFactor)
            self.max_radius = int(40.0/self.sizeFactor)
            self.con_max_radius = int(30.0/self.sizeFactor)
            self.con_min_radius = int(20.0/self.sizeFactor)
            self.param1 = 150
            self.param2 = 30
        elif lamp_type is "S30":
            self.min_radius = int(36.0/self.sizeFactor)
            self.max_radius = int(45.0/self.sizeFactor)
            self.con_max_radius = int(40.0/self.sizeFactor)
            self.con_min_radius = int(38.0/self.sizeFactor)
            self.param1 = 150
            self.param2 = 30
        elif lamp_type is "Misc":
            self.min_radius = int(115.0 / self.sizeFactor)
            self.max_radius = int(120.0 / self.sizeFactor)
            self.con_max_radius = int(110.0 / self.sizeFactor)
            self.con_min_radius = int(90.0 / self.sizeFactor)
            self.param1 = 150
            self.param2 = 20
        elif lamp_type is "MiscS30":
            self.min_radius = int(80.0 / self.sizeFactor)
            self.max_radius = int(85.0 / self.sizeFactor)
            self.con_max_radius = int(75.0 / self.sizeFactor)
            self.con_min_radius = int(65.0 / self.sizeFactor)
            self.param1 = 150
            self.param2 = 20
        self.rgb = []
        self.xy = []
        self.map = {}
        self.validSession = False
        self.fadeCount = 0
        self.numberOfLamps = no_of_lamps
        self.detectCircles()


    def detectCircles(self):
        circle_list = []
        for i in range(50):
            img = self.cam.resizedFrame()
            blur_img = cv2.medianBlur(img, 5)
            gray_img = cv2.cvtColor(blur_img, cv2.COLOR_RGB2GRAY)

            #Hough Circle detection, change the parameters if the circle isn't being detected
            try:
                self.circles = cv2.HoughCircles(gray_img, cv2.HOUGH_GRADIENT, 1, 20,
                                   param1=self.param1, param2=self.param2, minRadius=self.min_radius, maxRadius=self.max_radius)
            except:
                continue

            if self.circles is not None:
                for j in self.circles:
                    for i in j:
                        #print("Number of Circles: ",len(j),j)
                        circle_list.append(i)

        circle_list = np.asarray(circle_list)

        if(len(circle_list) == 0):
            raise ValueError("Try reconnecting the USB Cam/ restart the program : This can be because the lamp isn't in the view. If this repeats check the camera with camfeed.py")


        #Filtering the circles which might be from the same lamp, filtering with knn neighbor algorithm
        for k in range(self.lamp_count):
            KDTree = neighbors.KDTree(circle_list, leaf_size=self.lamp_count)
            ind= KDTree.query_radius([circle_list[k]],r = self.max_radius)[0]
            #indexes of the circles
            circle_list = np.delete(circle_list,np.asarray(ind[ind > k]),0)


        print(circle_list, "Length ", len(circle_list))
        self.circles = circle_list
        self.circles = np.uint16(np.around(self.circles))
        self.orderLamps()

        lamps = []
        for row in self.circles:
            for circle in row:
                lamps.append(circle)
        self.xy= [[] for i in range(len(lamps))]
        self.rgb = [[] for i in range(len(lamps))]
        self.expectedXY = [[] for i in range(len(lamps))]
        self.unmappedCircles = self.circles[:]

    def rgbToXY(self,color):
        r = color[2]
        g = color[1]
        b = color[0]
        converter = Converter()
        return list(converter.rgb_to_xy(r, g, b))

    def colorCheck(self,color, value):
        hsvValue = cv2.cvtColor(np.uint8([[value]]),cv2.COLOR_BGR2HSV)
        print("HSV Value: ",hsvValue)
        if color == 'RED':
            low = np.array([[[140,0,50]]],dtype=np.uint8)
            high = np.array([[[190, 255, 255]]], dtype=np.uint8)
            val = cv2.inRange(hsvValue,low,high)[0][0]
            if val == 255:
                return True
            else:
                low = np.array([[[0, 0, 50]]], dtype=np.uint8)
                high = np.array([[[15, 255, 255]]], dtype=np.uint8)
                val = cv2.inRange(hsvValue, low, high)[0][0]
                if val == 255:
                    return True
                else:
                    return False

    def getColor(self,framecount = 10,map=False,RGB=True, keepXY=None):
        sample = np.zeros((framecount, self.lamp_count, 3))
        circles = []
        if map:
            circles = self.unmappedCircles
        else:
            circles = self.circles
        for j in range(framecount):
            img = self.cam.resizedFrame()
            self.current = cv2.cvtColor(np.uint8(img),cv2.COLOR_BGR2GRAY)
            if img is None:
                break

            circle_index = 0
            for row in circles:
                for circle in row:
                    i = np.uint16(np.around(circle))
                    _color = ""
                    try:
                        if keepXY != None:
                            _color = self.concentric_mask(img,(i[0],i[1]),circle_index,RGB,keepXY)

                        else:
                            _color = self.concentric_mask(img, (i[0], i[1]), circle_index, RGB)

                        sample[j][circle_index] = _color

                    except:
                        print("Except in Getcolor")
                        continue

                    circle_index+=1
        #print(sample)
        frame_avg = np.mean(sample, axis=0)
        #total_avg = np.mean(frame_avg,axis=0)
        return frame_avg

    def mapDevice(self,color,deviceName):
        checkColor= []
        if color == "RED":
            checkColor = [150,150,255]
        if color == "BLUE":
            checkColor = [255,150,150]
        circleColors = self.getColor(10,True)
        #mappedIndex = self.closest_node(checkColor,circleColors)
        mappedIndex = 0
        redlampCount = 0
        for i in circleColors:
            if self.colorCheck("RED",i):
                self.map[deviceName] = mappedIndex
                redlampCount+=1
            mappedIndex+=1
        if(redlampCount == 0):
            return -1
        if(redlampCount > 1):
            return -2
        else:
            return self.map[deviceName]

    def concentric_mask(self, img, center, circle_index, RGB, keepXY=None):
        # Build mask
        mask = np.zeros(img.shape, dtype=np.uint8)
        cv2.circle(mask, center, self.con_max_radius, (255, 255, 255), -1, 8, 0)
        mask2 = np.full(img.shape, 255, dtype=np.uint8)
        cv2.circle(mask2, center, self.con_min_radius, (0, 0, 0), -1, 8, 0)
        img_array = img & mask & mask2
        # cv2.imshow("concentric",img_array)
        temp = img_array[img_array[:, :, 2] > 0.5]
        # rgbcolor = np.uint16(np.average(np.average(temp, axis=0), axis=0))
        rgbcolor = np.uint16(np.average(temp, axis=0))
        XY = self.rgbToXY(rgbcolor)
        if keepXY != None:
            self.expectedXY[circle_index].append(keepXY)
        if RGB:
            self.rgb[circle_index].append(rgbcolor)
            self.xy[circle_index].append(XY)
            return rgbcolor
        else:
            XY = self.rgbToXY(rgbcolor)
            self.xy[circle_index].append(XY)
            return XY

    def drawCircles(self,frame):
        for row in self.circles:
            for circle in row:
                print("Drawing Circle",circle)
                cv2.circle(frame,(circle[0],circle[1]),circle[2],(0,0,255),2)
        return frame

    def markLampNumbers(self,frame):
        font = cv2.FONT_HERSHEY_SIMPLEX
        i = 1
        for row in self.circles:
            print(row)
            for circle in row:
                print("Marking", circle)
                cv2.putText(frame, '%d'%i, (circle[0], circle[1]), font, 2, (255, 255, 255), 2, cv2.LINE_AA)
                cv2.drawMarker(frame,(circle[0],circle[1]),(255,0,0),markerType=cv2.MARKER_CROSS,markerSize=30,thickness=2,line_type=cv2.LINE_AA)
                i+=1
        #cv2.imwrite("MarkedFrame.jpg",frame)
        return frame

    def print_color(self, framecount=10, RGB=True):

        self.detectCircles()
        for j in range(int(framecount) - 1):
            img = self.cam.resizedFrame()
            if img is None:
                break
            #copy_curr_frame = curr_frame.copy()
            color = []
            circle_index = 0
            for row in self.circles:
                for circle in row:
                    i = np.uint16(np.around(circle))
                    try:
                        _color = self.concentric_mask(img, (i[0], i[1]), circle_index, RGB)
                        color.append(_color)
                        colorString = ['{:.3f}'.format(x) for x in _color]
                        print(colorString)
                        cv2.putText(img, str(colorString), (i[0] - i[2], i[1]), cv2.FONT_HERSHEY_SIMPLEX,
                                    0.5,
                                    255)
                        img = self.drawCircles(img)
                        img = self.markLampNumbers(img)

                    except:
                        print("PrintColor Exception")
                        continue

                    circle_index += 1
                    # _color = np.uint16(np.average(np.average(temp, axis=0),axis = 0))
                    # _color = self.rgbToXY(_color)
                    # cv2.imshow('Image  ', temp)
                    # cv2.waitKey(200)
            cv2.imshow('Image', img)
            cv2.waitKey(0)
        cv2.destroyAllWindows()

    def orderLamps(self):
        """
        Orders lamp by its geometrical position
        :return:
        """
        possibleLamps = []
        #radius filter
        print("Circles : ",self.circles)
        for circle in self.circles:
                if not (circle[2] < self.min_radius or circle[2] > self.max_radius):
                    possibleLamps.append(circle)

        #sort by x axis
        possibleLamps = sorted(possibleLamps , key=lambda k: [k[0], k[1]])
        x = [xy[0] for xy in possibleLamps]

        rows = self.splitByGroup(x)
        filteredRows = []
        for row in rows:
            filteredRow = []
            for x in row:
                i = 0
                for xy in possibleLamps:
                    if xy[0] == x:
                        possibleLamps.pop(i)
                        filteredRow.append(xy)
                    i += 1
            filteredRow = sorted(filteredRow, key=lambda  k: [k[1],k[0]])
            filteredRows.append(filteredRow)

        print("Number of Rows: ",len(rows),"Rows: ",filteredRows)
        self.filteredRows = filteredRows
        self.circles = self.filteredRows
        print("Circles now Equals to ",self.circles)

    def splitByGroupGenerator(self,lst, n):
        cluster = []
        for i in lst:
            if len(cluster) <= 1:  # the first two values are going directly in
                cluster.append(i)
                continue

            mean, stdev = self.stat(cluster)
            if abs(mean - i) > n * stdev:  # check the "distance"
                yield cluster
                cluster[:] = []  # reset cluster to the empty list
            cluster.append(i)

        yield cluster  # yield the last cluster

    def splitByGroup(self,filterElements):
        rows = []
        print(filterElements)
        for cluster in self.splitByGroupGenerator(filterElements, 4):
            print(cluster)
            rows.append(list(cluster))
        return rows

    def stat(self,lst):
        """Calculate mean and std deviation from the input list."""
        n = float(len(lst))
        mean = sum(lst) / n
        a =(sum(x * x for x in lst))
        stdev = sqrt(abs((sum(x * x for x in lst) / n) - (mean * mean)))
        return mean, stdev

    def timesyncCapture(self,seconds):
        if not self.validSession:
            self.validSession = True
            self.sessionID = str(uuid.uuid4())
        self.dataFrame = pd.DataFrame(columns =["Timestamp","Filename","ind"])


        now = datetime.datetime.now()
        future = now + datetime.timedelta(0, seconds=seconds)
        print(datetime.datetime.now())
        self.fadeCount += 1
        frameCount = 0
        self.rgb = [[] for i in range(self.numberOfLamps)]
        self.xy = [[] for i in range(self.numberOfLamps)]
        while (datetime.datetime.now() < future):
            #print(X,Y,Brightness)
            self.getColor(1)
            folder = 'indData/timeSyncData/'+ self.sessionID +"/fadeData/"
            filename = "%04d_%06d.jpg" % (self.fadeCount, frameCount)
            #filename = str(self.fadeCount)+'_'+str(frameCount)+".jpg"
            data = {"Timestamp": datetime.datetime.now(), "Filename": filename, "ind": frameCount}
            self.storeLampImagebyIndex(folder, filename)
            self.dataFrame = self.dataFrame.append(data, ignore_index=True)
            frameCount+=1
        self.dataFrame.to_csv(folder+"lookup.csv")
        hsv = cv2.cvtColor(np.uint8(self.rgb), cv2.COLOR_BGR2HSV)
        filename = 'indData/timeSyncData/'+self.sessionID +'/hsv_' + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S")) +'_'+str(0)+'_'+str(0)+'_'+str(0)+'_'+str(0)+'_'+str(seconds)+'_'+'.npy'
        filenameXY = 'indData/timeSyncData/'+self.sessionID +'/xy_' + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S")) +'_'+str(0)+'_'+str(0)+'_'+str(0)+'_'+str(0)+'_'+str(seconds)+'_'+'.npy'
        directory = os.path.dirname(filename)
        if not os.path.exists(directory):
            os.makedirs(directory)
        np.save(filename, hsv)
        np.save(filenameXY,self.xy)
        return frameCount

    def storeLampImagebyIndex(self,folder,filename):
        self.ROIDistance = self.max_radius
        index = 0
        for row in self.circles:
            for i in row:
                #print(i)
                print(self.currentImage.shape,i,self.ROIDistance)
                if int(i[1] - self.ROIDistance) > 0:
                    left = int(i[1] - self.ROIDistance)
                else:
                    left = 0

                if int(i[1] + self.ROIDistance) < self.currentImage.shape[0]:
                    right = int(i[1] + self.ROIDistance)
                else:
                    right = self.currentImage.shape[0]

                if int(i[0] - self.ROIDistance) > 0:
                    top = int(i[0] - self.ROIDistance)
                else:
                    top = 0

                if int(i[0] + self.ROIDistance) < self.currentImage.shape[1]:
                    bottom = int(i[0] + self.ROIDistance)
                else:
                    bottom = self.currentImage.shape[1]


                #roi = self.currentImage[int(i[1] - self.ROIDistance): int(i[1] + self.ROIDistance), int(i[0] - self.ROIDistance): int(i[0] + self.ROIDistance)]
                roi = self.currentImage[left: right,top: bottom]
                lampPath = folder + "/" + str(index) + "/" + filename
                directory = os.path.dirname(lampPath)
                if not os.path.exists(directory):
                    os.makedirs(directory)
                cv2.imwrite(lampPath, roi)
                #cv2.imshow('Image', roi)
                index+=1


if __name__ == "__main__":
    lampDetector = lampDetection(4,"S38")
    lampDetector.print_color(10)
    lampDetector.mapDevice("RED","Sample")
    #lampDetector.detectCircles()