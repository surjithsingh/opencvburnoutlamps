from twisted.internet.protocol import Protocol,Factory
from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.protocols.basic import LineReceiver
from lampDetection import lampDetection
import numpy as np
import pandas as pd
import os
import math
import json
from threading import Thread
from rgbxy import Converter
import time as Time
import uuid

class requestHandler(LineReceiver):
    def __init__(self,lampDetector):
        self.lampDetector = lampDetector
        #self.producer = Thread(target=self.cameraObj.producerGetColor, args=())
        #self.consumer = Thread(target=self.cameraObj.consumerGetColor, args=())
        self.test = False
        self.df = pd.DataFrame(columns=['x', 'y', 'img_X', 'img_Y'])
        print("Init")

    def lineReceived(self, data):
        #print(data)
        data = json.loads(data.decode("utf-8"))
        print(data)
        if data["cmd"] == "init":
            self.lampDetector.validSession = False
            self.lampDetector.sessionID = str(uuid.uuid4())
            response = {}
            response['response'] = "Initialized"
            response["UUID"] = self.lampDetector.sessionID
            response = json.dumps(response)
            self.transport.write(bytearray(response, 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if data["cmd"] == "reset":
            self.lampDetector.validSession = False
            response = {}
            response['response'] = "Session invalidated"
            response = json.dumps(response)
            self.transport.write(bytearray(response, 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if data["cmd"] == "map_device":
            serial_number = data["serial_number"]
            mappedIndex = self.lampDetector.mapDevice("RED", serial_number)
            response = {}
            if mappedIndex >= 0:
                response['response'] = "Success"
            else:
                response['response'] = "Failure"
            response["index"] = mappedIndex
            response = json.dumps(response)
            self.transport.write(bytearray(response, 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if data["cmd"] == "get_map_dict":
            maplist = [item for item in self.lampDetector.map.values() if item >= 0]
            status = False
            if len(set(maplist)) == len(self.lampDetector.map):
                status = True

            message = {"status":str(status),"map":self.lampDetector.map}

            self.transport.write(bytearray(json.dumps(message), 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if data["cmd"] == "read_xy":
            rgb = self.lampDetector.getColor(1)
            xy = self.lampDetector.rgbToXY(rgb[0])
            message = {"response": "Success", "x": str(xy[0]),"y": str(xy[1])}
            print(message)
            self.transport.write(bytearray(json.dumps(message), 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if  data["cmd"] == "timesync_capture":
            time = int(data["time"])
            frameCount = self.lampDetector.timesyncCapture(time)
            message = {"response": "Success", "frameCount":frameCount}
            print(message)
            self.transport.write(bytearray(json.dumps(message), 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))


class requestFactory(Factory):

    def __init__(self):
        print("factory")

        self.lampDetector = lampDetection(1, "MiscS30")
        self.lampDetector.detectCircles()

    def buildProtocol(self, addr):
        return requestHandler(self.lampDetector)



endpoint = TCP4ServerEndpoint(reactor, 13000)
endpoint.listen(requestFactory())
reactor.run()