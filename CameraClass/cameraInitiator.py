#Create Camera class on reception
#Takes care of the cameras used

import os
import sys
from twisted.internet.protocol import Protocol,Factory
from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.protocols.basic import LineReceiver
from lampDetection import lampDetection
#from colorChangeDetector import detectColors
import numpy as np
import pandas as pd
import os
import math
import json
from threading import Thread
from rgbxy import Converter
import time as Time

#available cameras

class cameraInitiator:
    def __init__(self,cameraObj):
        if cameraObj == 0:
            print("Resource Busy or error in detecting the lamp")

class requestFactory(Factory):
    def __init__(self):
        try:
            self.cameraObj = detectColors(0, True, "MiscS30", 1)
            self.cameraObj.calibrateShow()
            self.cameraObj.print_color()
        except:
            return 0
            # self.cameraObj = detectColors(0, True, "S38", 4)
        # self.cameraObj = detectColors(0, True, "S30", 8)

    def buildProtocol(self, addr):
        return cameraInitiator(self.cameraObj)


print(sys.argv)
numberOfLamps = int(sys.argv[1])
lampType = sys.argv[2]
print("Number of Lamps - ",numberOfLamps)
print("Lamp Type - ",lampType)


endpoint = TCP4ServerEndpoint(reactor, 13000)
endpoint.listen(requestFactory())
reactor.run()

