from twisted.internet.protocol import Protocol,Factory
from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.protocols.basic import LineReceiver
from colorChangeDetector import detectColors
import numpy as np
import pandas as pd
import os
import math
import json
from threading import Thread
from rgbxy import Converter
import time as Time

class requestHandler(LineReceiver):
    def __init__(self,cameraObj):

        self.cameraObj = cameraObj
        self.producer = Thread(target=self.cameraObj.producerGetColor, args=())
        self.consumer =Thread(target=self.cameraObj.consumerGetColor, args=())
        self.test = False
        self.df = pd.DataFrame(columns = ['x','y','img_X','img_Y'])

    def sendFile(self,filename,chunkSize):
        fileSize = os.stat(filename).st_size
        f = open(filename,'rb')
        transactions = math.ceil(float(fileSize)/float(chunkSize))
        header = {}
        header['fileName'] = filename
        header['fileSize'] = fileSize
        header['transactions'] = transactions
        header['chunkSize'] = chunkSize
        json_string = json.dumps(header)
        print(json_string)
        self.sendLine(bytearray(json_string, 'utf8'))
        '''
        self.setRawMode()
        while True:
            data = f.read(chunkSize)
            self.transport.write(data)
        self.setLineMode()
        #self.transport.write('\r\n')
        '''


    def lineReceived(self, data):
        data = data.decode("utf-8")
        print(data)
        if "initialize" in data:
            self.transport.write(bytearray("initialized", 'utf8'))

        if "get_frame_info" in data:
            frame = self.cameraObj.get_frame()
            filename = "stream.jpg"
            chunkSize = 2048
            fileSize = os.stat(filename).st_size

            transactions = math.ceil(float(fileSize) / float(chunkSize))
            header = {}
            header['fileName'] = filename
            header['fileSize'] = fileSize
            header['transactions'] = transactions
            header['chunkSize'] = chunkSize
            json_string = json.dumps(header)
            print(json_string)
            self.transport.write(bytearray(json_string,'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if "transfer_frame" in data:
            filename = "stream.jpg"
            f = open(filename, 'rb')
            filearray = f.read()
            self.setRawMode()
            chunks = [filearray[x:(x + 2048)] for x in range(0, len(filearray), 2048)]
            #chunks = [bytearray([i%256 for i in range(2048)]) for y in range(33)]
            for bytes in chunks:
                #print(chunks)
                self.transport.write(bytes)

            self.transport.write(bytearray('\0','utf8'))
            self.setLineMode()

        if "map_device" in data:
            print(data)
            cmdData = data.split('-')
            print(cmdData[1])
            mappedIndex = self.cameraObj.mapDevice("RED",cmdData[1])
            message = {"mapped":mappedIndex}
            self.transport.write(bytearray(json.dumps(message),'utf8'))
            print("sentResponse", cmdData[1],'-',mappedIndex)

        if "read_color" in data:
            cmdData = data.split('-')
            bgrcolor = self.cameraObj.readColorOfOneLamp(cmdData[1])
            color = {}
            color['Red'] = bgrcolor[2]
            color['Green'] = bgrcolor[1]
            color['Blue'] = bgrcolor[0]
            json_string = json.dumps(header)
            print(json_string)
            self.transport.write(bytearray(json_string, 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if "get_map_dict" in data:

            maplist = [item for item in self.cameraObj.map.values() if item >= 0]
            status = False
            if len(set(maplist)) == len(self.cameraObj.map):
                status = True
            message = {"status":str(status),"map":self.cameraObj.map}

            self.transport.write(bytearray(json.dumps(message), 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if "continous_read" in data:
            if self.test:
                self.cameraObj.map ={"KK":0,"KJ":1}
            maplist = [item for item in self.cameraObj.map.values() if item >= 0]
            print(self.cameraObj.map,maplist)

            if len(set(maplist)) == len(self.cameraObj.map):
                print("Map Success")
                self.producer.start()
                self.consumer.start()
                message = {"status": "started","reason":"continous_read"}
                self.transport.write(bytearray(json.dumps(message), 'utf8'))
                self.transport.write(bytearray('\n', 'utf8'))
            else:
                message = {"status": str(False), "reason": "check_map"}
                self.transport.write(bytearray(json.dumps(message), 'utf8'))
                self.transport.write(bytearray('\n', 'utf8'))

        if "stop_read" in data:
            self.cameraObj.stopGetColor = True
            self.stopThread()
            print("Stopped Thread")
            message = {"status": str(True), "reason": "stopped"}
            self.transport.write(bytearray(json.dumps(message), 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if "fade" in data:
            cmdData = data.split('-')
            time = int(cmdData[1]) + 2
            X = float(cmdData[2])
            Y = float(cmdData[3])
            brightness = float(cmdData[4])
            frameCount = self.cameraObj.fadeAcquire(data,time,X,Y,brightness)
            print("FrameCount: ",frameCount," FPS: ",frameCount/time)
            message = {"status": str(True), "reason": "Acquired"}
            self.transport.write(bytearray(json.dumps(message), 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))


        if "showGraph" in data:
            self.cameraObj.showGraph()
            message = {"status": str(True), "reason": "GraphPlotted"}
            self.transport.write(bytearray(json.dumps(message), 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))
            self.cameraObj.rgb = [[] for i in range(self.cameraObj.numberOfLamps)]
            self.cameraObj.expectedXY = [[] for i in range(self.cameraObj.numberOfLamps)]

        if "closeSession" in data:
            if self.cameraObj.validSession:
                self.cameraObj.storeLog()
                self.cameraObj.fadeCount = 0
                self.cameraObj.validSession = False

        if "scene" in data:
            cmdData = data.split('-')
            time = int(cmdData[1]) + 1
            scene1 = int(cmdData[2])
            scene2 = int(cmdData[3])
            brightness = int(cmdData[4])

            frameCount = self.cameraObj.sceneAcquire(data,time,scene1,scene2,brightness)
            print("FrameCount: ", frameCount, " FPS: ", frameCount / time)
            message = {"status": str(True), "reason": "Acquired"}
            self.transport.write(bytearray(json.dumps(message), 'utf8'))
            self.transport.write(bytearray('\n', 'utf8'))

        if "calibrate" in data:
            cmdData = data.split('-')
            time = int(cmdData[1])

            X = float(cmdData[2])
            Y = float(cmdData[3])
            cmd = cmdData[4]
            if cmd == "stop":
                self.cameraObj.saveData('Calibrate','calibration')
                print("Save data")
                self.df.to_csv("Calibration/calib.csv")

            Time.sleep(time)
            rgb = self.cameraObj.getColor(10)
            print(rgb[0])
            xy = self.cameraObj.rgbToXY(rgb[0])
            data = {"x":X,"y":Y,"img_X":xy[0],"img_Y":xy[1]}
            self.df = self.df.append(data, ignore_index=True)
            print(self.df)

        if "timesync_capture" in data:
            cmdData = data.split('-')
            time = int(cmdData[1])
            self.cameraObj.timesyncCapture(time)

        if "reset" in data:
            self.cameraObj.validSession = False
            self.cameraObj.fadeCount = 0



class requestFactory(Factory):

    def __init__(self):
        self.cameraObj = detectColors(0, True, "MiscS30", 1)
        #self.cameraObj = detectColors(0, True, "S38", 4)
        #self.cameraObj = detectColors(0, True, "S30", 8)
        self.cameraObj.calibrateShow()
        self.cameraObj.print_color()

    def buildProtocol(self, addr):
        return requestHandler(self.cameraObj)



endpoint = TCP4ServerEndpoint(reactor, 13000)
endpoint.listen(requestFactory())
reactor.run()