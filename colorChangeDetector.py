import cv2
import numpy as np
import pandas as pd
from math import sqrt
import statistics
import itertools
from collections import Counter
import scipy
from sklearn import neighbors
from rgbxy import Converter
import pymysql
from datetime import datetime
import json
import threading
import logging
from multiprocessing import Queue
import datetime
import uuid
import os

logging.basicConfig(level=logging.DEBUG,format='(%(threadName)-9s) %(message)s',)


class detectColors:
    def __init__(self, _filename,_usecam,lampType = "S30",numberofLamps = 8,HighResolution = False,UI = False):
        self.orderedCircles = []
        self.filename = _filename
        self.Q = Queue(1000)
        self.stopGetColor = False;
        if _usecam:
            self.video = cv2.VideoCapture(_filename)
            self.cam = True
            camSuccess,Frame = self.video.read()
            while not camSuccess:
                print("Trying to reconnect the camera")
                self.video.release()
                self.video = cv2.VideoCapture(_filename)
                camSuccess,frame = self.video.read()
            print("Setting Resolution")
            self.video.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
            self.video.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
            self.video.set(cv2.CAP_PROP_FPS, 120)
        else:
            self.video = cv2.VideoCapture(_filename)
            self.cam = False

        try:
            self.sql_conn = pymysql.connect(host='127.0.0.1', port=3306, user='root', passwd='suji_123', db='testcameradb')
            self.sql_cursor = self.sql_conn.cursor()
        except:
            print("Database Connection failed")
        self.numberOfLamps = numberofLamps
        self.framecount = self.video.get(cv2.CAP_PROP_FRAME_COUNT) - 5
        self.fps = self.video.get(cv2.CAP_PROP_FPS)
        self.UI = UI
        if HighResolution:
            self.resize = 640.0
            self.sizeFactor = 1.0
        else:
            self.resize = 320.0
            self.sizeFactor = 2.0
        if lampType is "S38":
            self.min_radius = int(50.0/self.sizeFactor)
            self.max_radius = int(65.0/self.sizeFactor)
            self.conMaxRadius = int(45.0/self.sizeFactor)
            self.conMinRadius = int(35.0/self.sizeFactor)
            self.param1 = 150
            self.param2 = 30
        elif lampType is "PAR":
            self.min_radius = int(20.0/self.sizeFactor)
            self.max_radius = int(40.0/self.sizeFactor)
            self.conMaxRadius = int(30.0/self.sizeFactor)
            self.conMinRadius = int(20.0/self.sizeFactor)
            self.param1 = 150
            self.param2 = 30
        elif lampType is "S30":
            self.min_radius = int(36.0/self.sizeFactor)
            self.max_radius = int(45.0/self.sizeFactor)
            self.conMaxRadius = int(40.0/self.sizeFactor)
            self.conMinRadius = int(38.0/self.sizeFactor)
            self.param1 = 150
            self.param2 = 30
        elif lampType is "Misc":
            self.min_radius = int(220.0 / self.sizeFactor)
            self.max_radius = int(230.0 / self.sizeFactor)
            self.conMaxRadius = int(210.0 / self.sizeFactor)
            self.conMinRadius = int(170.0 / self.sizeFactor)
            self.param1 = 150
            self.param2 = 20
        elif lampType is "MiscS30":
            self.min_radius = int(180.0 / self.sizeFactor)
            self.max_radius = int(220.0 / self.sizeFactor)
            self.conMaxRadius = int(105.0 / self.sizeFactor)
            self.conMinRadius = int(75.0 / self.sizeFactor)
            self.param1 = 150
            self.param2 = 20

        self.lampType = lampType
        self.roi = []
        self.circles = []
        self.filteredRows = []
        self.currentImage = None
        self.prev = None
        self.current = None
        self.xy = []
        self.rgb = []
        self.expectedXY = []
        self.map = {}
        self.unmappedCircles = []
        self.fadeCount = 0
        self.validSession = False
        self.sessionID = None
        self.sessionCmds = ""
        self.scene = None

    def __del__(self):
        self.video.release()
        #self.out.release()
        cv2.destroyAllWindows()

    def joinCam(self):
        self.video = cv2.VideoCapture(self.filename)
        self.cam = True
        camSuccess, Frame = self.video.read()
        while not camSuccess:
            print("Trying to reconnect the camera")
            self.video.release()
            self.video = cv2.VideoCapture(self.filename)
            camSuccess, frame = self.video.read()

    def releaseCam(self):
        self.video.release()



    def detect_circles(self):
        np_sample = []
        for i in range(100):
            ret, img = self.video.read()
            if not ret:
                continue
            r = self.resize / img.shape[1]
            dim = (int(self.resize), int(img.shape[0] * r))
            # perform the actual resizing of the image and show it
            img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
            #img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
            img = cv2.medianBlur(img, 5)
            cimg = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            #print(cimg.shape)


            try:
                self.circles = cv2.HoughCircles(cimg, cv2.HOUGH_GRADIENT, 1, 20,
                                   param1=self.param1, param2=self.param2, minRadius=self.min_radius, maxRadius=self.max_radius)
            except:
                continue
            if self.circles is not None:
                for j in self.circles:
                    for i in j:
                        print("Number of Circles: ",len(j),j)
                        np_sample.append(i)

        np_sample = np.asarray(np_sample)
        print(np_sample,"Length ", len(np_sample))
        if(len(np_sample) == 0):
            raise ValueError("Try reconnecting the USB Cam/ restart the program")

        for k in range(self.numberOfLamps):
            KDTree = neighbors.KDTree(np_sample, leaf_size=self.numberOfLamps)
            print(k,np_sample)
            print("Sample close to ", np_sample[k])
            a = KDTree.query_radius([np_sample[k]],r = self.max_radius)
            [ind] = a
            #print("Indices of neighbors", ind)
            #print("deleting ",ind)
            np_sample = np.delete(np_sample,np.asarray(ind[ind > k]),0)
            #print(np_sample, "Length ", len(np_sample))

        print(np_sample, "Length ", len(np_sample))
        self.circles = np_sample
        print(self.circles)
        self.circles = np.uint16(np.around(self.circles))
        print(self.circles)
        self.orderLamps()

        lamps = []
        for row in self.circles:
            for circle in row:
                lamps.append(circle)
        self.xy= [[] for i in range(len(lamps))]
        self.rgb = [[] for i in range(len(lamps))]
        self.expectedXY = [[] for i in range(len(lamps))]
        self.unmappedCircles = self.circles[:]


    def resized_frame(self):
        ret, img = self.video.read()
        if not ret:
            return None
        else:
            r = self.resize / img.shape[1]
            dim = (int(self.resize), int(img.shape[0] * r))
            # perform the actual resizing of the image and show it
            self.currentImage = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
            return self.currentImage

    def color_change(self):
        curr_frame = []
        prev_frame = []
        print(self.framecount)
        for i in range(int(self.framecount) - 1):
            curr_frame = self.resized_frame()
            if curr_frame is None:
                break
            if i > 0:
                diff = np.abs(np.subtract(prev_frame, curr_frame))
                #cv2.imshow('Image',diff)
                cv2.waitKey(20)

                #self.out.write(diff)
            prev_frame = curr_frame


    def concentric_mask(self,img,center,circle_index):
        # Build mask
        mask = np.zeros(img.shape, dtype=np.uint8)
        cv2.circle(mask,center, self.conMaxRadius, (255, 255, 255), -1, 8, 0)
        #mask2 = np.full(img.shape, 255, dtype=np.uint8)
        #cv2.circle(mask2, center, self.conMinRadius, (0, 0, 0), -1, 8, 0)
        img_array = img & mask
        #cv2.imshow("concentric",img_array)
        temp = img_array[img_array[:, :, 2] > 0.5]
        #rgbcolor = np.uint16(np.average(np.average(temp, axis=0), axis=0))
        rgbcolor = np.uint16(np.average(temp, axis=0))
        #print(rgbcolor)
        XY = self.rgbToXY(rgbcolor)
        self.xy[circle_index].append(XY)
        return XY

    def concentric_mask(self,img,center,circle_index,RGB,keepXY=None):
        # Build mask
        mask = np.zeros(img.shape, dtype=np.uint8)
        cv2.circle(mask,center, self.conMaxRadius, (255, 255, 255), -1, 8, 0)
        mask2 = np.full(img.shape, 255, dtype=np.uint8)
        cv2.circle(mask2, center, self.conMinRadius, (0, 0, 0), -1, 8, 0)
        img_array = img & mask & mask2
        #cv2.imshow("concentric",img_array)
        temp = img_array[img_array[:, :, 2] > 0.5]
        #rgbcolor = np.uint16(np.average(np.average(temp, axis=0), axis=0))
        rgbcolor = np.uint16(np.average(temp, axis=0))
        XY = self.rgbToXY(rgbcolor)
        if keepXY != None:
            self.expectedXY[circle_index].append(keepXY)
        if RGB:
            self.rgb[circle_index].append(rgbcolor)
            self.xy[circle_index].append(XY)
            return rgbcolor
        else:
            XY = self.rgbToXY(rgbcolor)
            self.xy[circle_index].append(XY)
            return XY



    def getColor(self,framecount = 10,map=False,RGB=True, keepXY=None):
        sample = np.zeros((framecount, self.numberOfLamps, 3))
        circles = []
        if map:
            circles = self.unmappedCircles
        else:
            circles = self.circles
        for j in range(framecount):
            curr_frame = self.resized_frame()
            self.current = cv2.cvtColor(np.uint8(curr_frame),cv2.COLOR_BGR2GRAY)
            if curr_frame is None:
                break
            copy_curr_frame = curr_frame.copy()
            color = []
            circle_index = 0

            cv2.waitKey(2)
            for row in circles:
                for circle in row:
                    i = np.uint16(np.around(circle))
                    _color = ""
                    try:
                        if keepXY != None:
                            _color = self.concentric_mask(curr_frame,(i[0],i[1]),circle_index,RGB,keepXY)

                        else:
                            _color = self.concentric_mask(curr_frame, (i[0], i[1]), circle_index, RGB)

                        #print(_color)
                        sample[j][circle_index] = _color

                    except:
                        print("Except in printcolor")
                        continue

                    circle_index+=1
        #print(sample)
        frame_avg = np.mean(sample, axis=0)
        total_avg = np.mean(frame_avg,axis=0)
        #print("Frame Avg",frame_avg)
        #print("Total Avg", total_avg)
        return frame_avg

    def testGetColor(self):
        while True:
            a = input("Enter to Getcolor")
            if a == "Q":
                break
            else:
                print(self.getColor(20))

    def closest_node(self,node,nodes):
        nodes = np.asarray(nodes)
        deltas = nodes - node
        dist_2 = np.einsum('ij,ij->i', deltas, deltas)
        print(dist_2)
        mappedIndex = np.argmin(dist_2)
        '''
        mappedIndex = np.argwhere(dist_2 < 10000)
        mappedIndex.flatten()
        if len(mappedIndex) > 1:
            raise "Make Sure to light up one New Lamp"
        '''
        '''
        k = 0
        i = 0
        for row in self.unmappedCircles:
            j = 0
            for circle in row:
                if i == mappedIndex:
                    #self.unmappedCircles[k].pop(j)
                i+=1
                j+=1
            k+=1
        '''
        return mappedIndex

    def get_frame(self):
        s,img = self.video.read()
        if s:
            print("Writing image to file")
            cv2.imwrite("stream.jpg",img)
        else:
            print("Capture Failed")


    def mapDevice(self,color,deviceName):
        checkColor= []
        if color == "RED":
            checkColor = [150,150,255]
        if color == "BLUE":
            checkColor = [255,150,150]
        circleColors = self.getColor(10,True)
        #mappedIndex = self.closest_node(checkColor,circleColors)
        mappedIndex = 0
        redlampCount = 0
        for i in circleColors:
            if self.colorCheck("RED",i):
                self.map[deviceName] = mappedIndex
                redlampCount+=1
            mappedIndex+=1
        if(redlampCount == 0):
            return -1
        if(redlampCount > 1):
            return -2
        else:
            return self.map[deviceName]


    def colorCheck(self,color, value):
        hsvValue = cv2.cvtColor(np.uint8([[value]]),cv2.COLOR_BGR2HSV)
        print("HSV Value: ",hsvValue)
        if color == 'RED':
            low = np.array([[[140,0,50]]],dtype=np.uint8)
            high = np.array([[[190, 255, 255]]], dtype=np.uint8)
            val = cv2.inRange(hsvValue,low,high)[0][0]
            if val == 255:
                return True
            else:
                low = np.array([[[0, 0, 50]]], dtype=np.uint8)
                high = np.array([[[15, 255, 255]]], dtype=np.uint8)
                val = cv2.inRange(hsvValue, low, high)[0][0]
                if val == 255:
                    return True
                else:
                    return False

    def continousCheck(self):
        self.inverseMap = dict((y, x) for x, y in self.map.items())
        print(self.inverseMap)
        print(self.map.items)
        sql_insert_cmd = """INSERT INTO testcameradb.LampColor VALUES (%s, %s, %s, %s)"""
        for i in range(1000):
            colors = self.getColor(1,True)
            timestamp = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]
            j = 0;
            db_body = {}
            for i in colors:
                serialNumber = self.inverseMap[j]
                color_json = {'R': int(i[2]),'G':int(i[1]),'B':int(i[0])}
                event_json = {'function': 'get_color'}
                sql_args = (serialNumber,timestamp,json.dumps(color_json),json.dumps(event_json))
                self.sql_cursor.execute(sql_insert_cmd,sql_args)
                j+=1
            self.sql_conn.commit()

    def consumerGetColor(self):
        print("Inside Consumer")
        self.inverseMap = dict((y, x) for x, y in self.map.items())
        print(self.inverseMap)
        print(self.map.items)
        sql_insert_cmd = """INSERT INTO testcameradb.LampColor VALUES (%s, %s, %s, %s)"""

        x = np.empty((len(self.inverseMap),500))
        y = np.empty((len(self.inverseMap),500))
        timestamp = [None]*500

        for j in range(500):
            message = self.Q.get()
            lamp_index = 0
            for i in message['colors']:
                xy = self.rgbToXY(i)
                x[lamp_index][j] = xy[0]
                y[lamp_index][j] = xy[1]
                lamp_index+=1
            timestamp[j] = message['timestamp']

        frame_index = 0
        while not self.stopGetColor:
            print("Inside Consumer",self.stopGetColor)
            if self.stopGetColor:
                return
            else:
                frame_index += 1
                frame_index %= 500
                lamp_index = 0
                message = self.Q.get()
                for i in message['colors']:
                    serialNumber = self.inverseMap[lamp_index]
                    color_json = {'R': int(i[2]), 'G': int(i[1]), 'B': int(i[0])}
                    event_json = {'function': 'get_color'}
                    if(message['flicker'][lamp_index]):
                        event_json = {'function': 'flicker'}
                        sql_args = (serialNumber, message['timestamp'], json.dumps(color_json), json.dumps(event_json))
                        self.sql_cursor.execute(sql_insert_cmd, sql_args)
                        print(sql_args)

                    lamp_index+=1

                self.sql_conn.commit()


                lamp_index = 0
                for i in message['colors']:
                    xy = self.rgbToXY(i)
                    x[lamp_index][frame_index] = xy[0]
                    y[lamp_index][frame_index] = xy[1]
                timestamp[frame_index] = message['timestamp']

    def stopThread(self):
        self.stopGetColor = True

    def producerGetColor(self):
        colors = self.getColor(1, True)
        self.prev = self.current.copy()
        while not self.stopGetColor:
            #print("Inside Producer",self.stopGetColor)
            if self.stopGetColor:
                return
            else:
                colors = self.getColor(1, True)
                flicker = self.flickerCV()
                timestamp = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]
                message = {'colors':colors,'timestamp':timestamp,'flicker':flicker}
                self.Q.put(message)

    def flickerCheck(self,x,y):
        x_std = x.std()
        y_std = y.std()
        x_mean = x.mean()
        y_mean = y.mean()
        print(x_std,y_std)
        if (x_std > 0.02) or (y_std > 0.02):
            x_deviation = x_mean + (2*x_std)
            y_deviation = y_mean + (2 * y_std)
            return True
        else:
            return False


    def flickerCV(self):
        diff = cv2.absdiff(self.prev,self.current)
        self.prev = self.current.copy()
        cv2.imshow("Diff",diff)
        cv2.waitKey(10)
        flicker = [None]*self.numberOfLamps
        lampIndex = 0
        for row in self.circles:
            for i in row:
                roi = diff[i[1]-(int(i[2])-5): i[1]+(int(i[2])+5),i[0]-(int(i[2])-5): i[0]+(int(i[2])+5)]
                #roi = diff[77: 107,209: 234]
                ret,thresh = cv2.threshold(roi,50,255,cv2.THRESH_BINARY)
                displayName = "roi" + str(lampIndex)
                #cv2.imshow(displayName,thresh)
                #cv2.waitKey(10)
                count = cv2.countNonZero(thresh)
                if count > 100:
                    print("Flickering ", count)
                    flicker[lampIndex] = True
                else:
                    flicker[lampIndex] = False
                lampIndex+=1
        return flicker

    def fadeAcquire(self,cmd,seconds,X=0,Y=0,Brightness=0):
        self.scene = False
        self.sessionCmds += cmd+'\n'
        if not self.validSession:
            self.validSession = True
            self.sessionID = str(uuid.uuid4())


        now = datetime.datetime.now()
        future = now + datetime.timedelta(0, seconds=seconds)
        print(datetime.datetime.now())
        self.fadeCount+=1
        frameCount = 0
        self.rgb = [[] for i in range(self.numberOfLamps)]
        self.xy = [[] for i in range(self.numberOfLamps)]
        while (datetime.datetime.now() < future):
            #print(X,Y,Brightness)
            self.getColor(1,keepXY=[X,Y,Brightness])

            folder = 'indData/' + self.sessionID + "/fadeData/"
            filename = "%04d_%06d.jpg"%(self.fadeCount,frameCount)
            #filename = str(self.fadeCount) + '_' + str(frameCount) + ".jpg"
            self.storeLampImagebyIndex(folder, filename)
            '''
            frameName =  'indData/'+ self.sessionID +"/fadeData/"+str(self.fadeCount)+'_'+str(frameCount)+".jpg"
            directory = os.path.dirname(frameName)
            if not os.path.exists(directory):
                os.makedirs(directory)
            cv2.imwrite(frameName,self.currentImage)
            '''
            frameCount+=1

        hsv = cv2.cvtColor(np.uint8(self.rgb), cv2.COLOR_BGR2HSV)
        filename = 'indData/'+self.sessionID +'/hsv_' + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S")) +'_'+ str(X)+'_'+str(Y)+'_'+str(Brightness)+'_'+str(seconds)+'_'+'.npy'
        filenameXY = 'indData/' + self.sessionID + '/xy_' + str(
            datetime.datetime.now().strftime("%Y%m%d-%H%M%S")) + '_' + str(X) + '_' + str(Y) + '_' + str(
            Brightness) + '_' + str(seconds) + '_' + '.npy'
        directory = os.path.dirname(filename)
        if not os.path.exists(directory):
            os.makedirs(directory)
        np.save(filename, hsv)
        np.save(filenameXY,self.xy)
        return frameCount

    def saveData(self,folderName,file):
        if not self.validSession:
            self.validSession = True
            self.sessionID = str(uuid.uuid4())

        hsv = cv2.cvtColor(np.uint8(self.rgb), cv2.COLOR_BGR2HSV)
        filename = 'indData/' +folderName+'/' +self.sessionID + '/'+file+"_" + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))+'.npy'
        filenameXY = 'indData/' +folderName+'/' +self.sessionID + '/'+file+"_xy" + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))+'.npy'
        directory = os.path.dirname(filename)
        if not os.path.exists(directory):
            os.makedirs(directory)
        np.save(filename, hsv)
        np.save(filenameXY, self.xy)
        self.rgb = [[] for i in range(self.numberOfLamps)]
        self.xy = [[] for i in range(self.numberOfLamps)]
        self.validSession = False


    def storeLampImagebyIndex(self,folder,filename):
        self.ROIDistance = self.max_radius
        index = 0
        for row in self.circles:
            for i in row:
                #print(i)
                print(self.currentImage.shape,i,self.ROIDistance)
                if int(i[1] - self.ROIDistance) > 0:
                    left = int(i[1] - self.ROIDistance)
                else:
                    left = 0

                if int(i[1] + self.ROIDistance) < self.currentImage.shape[0]:
                    right = int(i[1] + self.ROIDistance)
                else:
                    right = self.currentImage.shape[0]

                if int(i[0] - self.ROIDistance) > 0:
                    top = int(i[0] - self.ROIDistance)
                else:
                    top = 0

                if int(i[0] + self.ROIDistance) < self.currentImage.shape[1]:
                    bottom = int(i[0] + self.ROIDistance)
                else:
                    bottom = self.currentImage.shape[1]


                #roi = self.currentImage[int(i[1] - self.ROIDistance): int(i[1] + self.ROIDistance), int(i[0] - self.ROIDistance): int(i[0] + self.ROIDistance)]
                roi = self.currentImage[left: right,top: bottom]
                lampPath = folder + "/" + str(index) + "/" + filename
                directory = os.path.dirname(lampPath)
                if not os.path.exists(directory):
                    os.makedirs(directory)
                cv2.imwrite(lampPath, roi)
                #cv2.imshow('Image', roi)
                index+=1



    def storeLog(self):
        if self.scene:
            folder = 'indData/sceneData/' + self.sessionID + "/fadeData/"
        else:
            folder = 'indData/' + self.sessionID + "/fadeData/"

        filename = 'cmd.txt'
        with open(folder+filename, "w") as text_file:
            text_file.write(self.sessionCmds)
        self.sessionCmds = ""


    def timesyncCapture(self,seconds):
        if not self.validSession:
            self.validSession = True
            self.sessionID = str(uuid.uuid4())
        self.dataFrame = pd.DataFrame(columns =["Timestamp","Filename","ind"])


        now = datetime.datetime.now()
        future = now + datetime.timedelta(0, seconds=seconds)
        print(datetime.datetime.now())
        self.fadeCount += 1
        frameCount = 0
        self.rgb = [[] for i in range(self.numberOfLamps)]
        self.xy = [[] for i in range(self.numberOfLamps)]
        while (datetime.datetime.now() < future):
            #print(X,Y,Brightness)
            self.getColor(1)
            folder = 'indData/timeSyncData/'+ self.sessionID +"/fadeData/"
            filename = "%04d_%06d.jpg" % (self.fadeCount, frameCount)
            #filename = str(self.fadeCount)+'_'+str(frameCount)+".jpg"
            data = {"Timestamp": datetime.datetime.now(), "Filename": filename, "ind": frameCount}
            self.storeLampImagebyIndex(folder, filename)
            self.dataFrame = self.dataFrame.append(data, ignore_index=True)
            frameCount+=1
        self.dataFrame.to_csv(folder+"lookup.csv")
        hsv = cv2.cvtColor(np.uint8(self.rgb), cv2.COLOR_BGR2HSV)
        filename = 'indData/timeSyncData/'+self.sessionID +'/hsv_' + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S")) +'_'+str(0)+'_'+str(0)+'_'+str(0)+'_'+str(0)+'_'+str(seconds)+'_'+'.npy'
        filenameXY = 'indData/timeSyncData/'+self.sessionID +'/xy_' + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S")) +'_'+str(0)+'_'+str(0)+'_'+str(0)+'_'+str(0)+'_'+str(seconds)+'_'+'.npy'
        directory = os.path.dirname(filename)
        if not os.path.exists(directory):
            os.makedirs(directory)
        np.save(filename, hsv)
        np.save(filenameXY,self.xy)
        return frameCount


    def sceneAcquire(self,cmd,seconds,scene1=0,scene2=0,Brightness=0):
        self.scene = True
        self.sessionCmds += cmd+'\n'
        if not self.validSession:
            self.validSession = True
            self.sessionID = str(uuid.uuid4())

        now = datetime.datetime.now()
        future = now + datetime.timedelta(0, seconds=seconds)
        print(datetime.datetime.now())
        self.fadeCount+=1
        frameCount = 0
        self.rgb = [[] for i in range(self.numberOfLamps)]
        self.xy = [[] for i in range(self.numberOfLamps)]
        while (datetime.datetime.now() < future):
            #print(X,Y,Brightness)
            self.getColor(1)
            folder = 'indData/sceneData/'+ self.sessionID +"/fadeData/"
            filename = "%04d_%06d.jpg" % (self.fadeCount, frameCount)
            #filename = str(self.fadeCount)+'_'+str(frameCount)+".jpg"
            self.storeLampImagebyIndex(folder, filename)
            '''
            frameName =  'indData/sceneData/'+ self.sessionID +"/fadeData/"+str(self.fadeCount)+'_'+str(frameCount)+".jpg"
            directory = os.path.dirname(frameName)
            if not os.path.exists(directory):
                os.makedirs(directory)
            cv2.imwrite(frameName,self.currentImage)
            '''
            frameCount+=1

        hsv = cv2.cvtColor(np.uint8(self.rgb), cv2.COLOR_BGR2HSV)
        filename = 'indData/sceneData/'+self.sessionID +'/hsv_' + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S")) +'_'+ str(scene1)+'_'+str(scene2)+'_'+str(Brightness)+'_'+str(seconds)+'_'+'.npy'
        filenameXY = 'indData/sceneData/'+self.sessionID +'/xy_' + str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S")) +'_'+ str(scene1)+'_'+str(scene2)+'_'+str(Brightness)+'_'+str(seconds)+'_'+'.npy'
        directory = os.path.dirname(filename)
        if not os.path.exists(directory):
            os.makedirs(directory)
        np.save(filename, hsv)
        np.save(filenameXY,self.xy)
        return frameCount

    def patternMatch(self,testData,index):
        refData = np.load("refS38.npy")
        if self.lampType == "S30":
            refData = np.mod(refData,175)
        elif self.lampType == "S38":
            refData = np.mod(refData, 162)
        hRef = [None] * (refData.shape[0])
        hTest = [None] * (testData.shape[0])

        for i in range(refData.shape[0]):
            hRef[i] = [h_[index] for h_ in refData[i]]

        for i in range(testData.shape[0]):
            hTest[i] = [h_[index] for h_ in testData[i]]

        hRef = np.array(hRef)
        hTest = np.array(hTest)
        hRef = np.mod(hRef,175)
        hTest = np.mod(hTest,175)

        matchValue = np.ndarray((refData.shape[0],testData.shape[0]))
        for i in range(refData.shape[0]):
            for j in range(testData.shape[0]):
                g_ = None
                h_ = None
                if hRef[i].shape[0] < hTest[j].shape[0]:
                    g_ = hRef[i][:hRef[i].shape[0]]
                    h_ = hTest[j][:hRef[i].shape[0]]
                elif hTest[j].shape[0] < hRef[i].shape[0]:
                    g_ = hRef[i][:hTest[j].shape[0]]
                    h_ = hTest[j][:hTest[j].shape[0]]
                else:
                    g_ = hRef[i]
                    h_ = hTest[j]
                mat = np.corrcoef(g_, h_)
                matchValue[i][j] = mat[0][1]
                print(mat[0][1], i, j)
        lampMatchValue = np.ndarray(testData.shape[0])
        for i in range(testData.shape[0]):
            lampMatchValue[i] = np.amax(matchValue[:,i])

        return lampMatchValue

    def showGraph(self,show = True,patternMatch = False):
        if self.validSession:
            self.validSession = False
        import matplotlib.pyplot as plt
        hsv = cv2.cvtColor(np.uint8(self.rgb), cv2.COLOR_BGR2HSV)

        if(self.numberOfLamps < 8):
            fig, ax = plt.subplots(nrows=int(self.numberOfLamps/2 + 2), ncols=int(self.numberOfLamps%2 + 2))
            fig2, ax2 = plt.subplots(nrows=int(self.numberOfLamps/2 + 2), ncols=int(self.numberOfLamps%2 + 2))
            fig3, ax3 = plt.subplots(nrows=int(self.numberOfLamps/2 + 2), ncols=int(self.numberOfLamps%2 + 2))
        else:
            fig, ax = plt.subplots(nrows=int(self.numberOfLamps / 2), ncols=int(self.numberOfLamps / 4))
            fig2, ax2 = plt.subplots(nrows=int(self.numberOfLamps / 2), ncols=int(self.numberOfLamps / 4))
            fig3, ax3 = plt.subplots(nrows=int(self.numberOfLamps / 2), ncols=int(self.numberOfLamps / 4))


        filename = 'dataHSV/hsv_'+str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))+'.npy'

        np.save(filename,hsv)


        if show:

            if self.lampType == "S30":
                hsv = np.mod(hsv,175)
            elif self.lampType == "S38":
                hsv = np.mod(hsv, 162)

            h_Value = [0]*self.numberOfLamps
            s_Value = [0]*self.numberOfLamps
            v_Value = [0]*self.numberOfLamps
            if patternMatch:
                h_Value = self.patternMatch(hsv,0)
                s_Value = self.patternMatch(hsv, 1)
                v_Value = self.patternMatch(hsv, 2)


            for i in range(list(hsv.shape)[0]):
                h = [h_[0] for h_ in hsv[i]]
                s = [s_[1] for s_ in hsv[i]]
                v = [v_[2] for v_ in hsv[i]]
                j = np.array([k for k in range(len(hsv[i]))])

                text = "Match - "+str(h_Value[i])
                print(i)
                ax[int(i / 2)][i % 2].set_ylim([0, 200])
                ax[int(i / 2)][i % 2].plot(j, h, label='Lamp %d' % (i + 1))
                ax[int(i / 2)][i % 2].legend()
                ax[int(i / 2)][i % 2].text(0.95,0.01, text, style='italic',
                        bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})

                text = "Match - " + str(s_Value[i])
                ax2[int(i / 2)][i % 2].set_ylim([0, 255])
                ax2[int(i / 2)][i % 2].plot(j, s, label='Lamp %d' % (i + 1))
                ax2[int(i / 2)][i % 2].legend()
                ax2[int(i / 2)][i % 2].text(0.95, 0.01, text, style='italic',
                                           bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})

                text = "Match - " + str(v_Value[i])
                ax3[int(i / 2)][i % 2].set_ylim([0, 255])
                ax3[int(i / 2)][i % 2].plot(j, v, label='Lamp %d' % (i + 1))
                ax3[int(i / 2)][i % 2].legend()
                ax3[int(i / 2)][i % 2].text(0.95, 0.01, text, style='italic',
                                           bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})

            hsv = self.XYToHSV()
            if (self.numberOfLamps < 8):
                fig1, ax1 = plt.subplots(nrows=int(self.numberOfLamps/2 + 2), ncols=int(self.numberOfLamps%2 + 2))
            else:
                fig1, ax1 = plt.subplots(nrows=int(self.numberOfLamps / 2), ncols=int(self.numberOfLamps / 4))

            for i in range(list(hsv.shape)[0]):
                h = [h_[0] for h_ in hsv[i]]
                s = [s_[1] for s_ in hsv[i]]
                v = [v_[2] for v_ in hsv[i]]
                j = np.array([k for k in range(len(hsv[i]))])
                ax1[int(i / 2)][i % 2].set_ylim([0,200])
                ax1[int(i / 2)][i % 2].plot(j, h, label='Lamp %d' % (i + 1))
                ax1[int(i / 2)][i % 2].legend()

            plt.show()


    def readColorOfOneLamp(self,deviceName):
        mappedIndex = self.map[deviceName]
        circleColors = self.getColor(10,True)
        return circleColors[mappedIndex]



    def print_color(self,framecount = 10,RGB=True):
        if(self.cam):
            self.framecount = framecount

        self.detect_circles()
        for j in range(int(self.framecount) - 1):
            curr_frame = self.resized_frame()
            if curr_frame is None:
                break
            copy_curr_frame = curr_frame.copy()
            color = []
            circle_index = 0
            for row in self.circles:
                for circle in row:
                    i = np.uint16(np.around(circle))
                    #print(i)
                    #img[i[1]-(int(i[2]/2)): i[1]+(int(i[2]/2)),i[0]-(int(i[2]/2)): i[0]+(int(i[2]/2))]
                    #temp = curr_frame[i[1]-(int(i[2]*0.707)): i[1]+(int(i[2]*0.707)),i[0]-(int(i[2]*0.707)): i[0]+(int(i[2]*0.707))]
                    _color = ""
                    try:
                        _color = self.concentric_mask(curr_frame,(i[0],i[1]),circle_index,RGB)
                        color.append(_color)
                        colorString = ['{:.3f}'.format(x) for x in _color]
                        print(colorString)
                        cv2.putText(copy_curr_frame, str(colorString), (i[0] - i[2], i[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                                    255)
                        copy_curr_frame = self.drawCircles(copy_curr_frame)
                        copy_curr_frame = self.markLampNumbers\
                            (copy_curr_frame)

                    except:
                        print("Except in printcolor")
                        continue

                    circle_index+=1
                    #_color = np.uint16(np.average(np.average(temp, axis=0),axis = 0))
                    #_color = self.rgbToXY(_color)
                    #cv2.imshow('Image  ', temp)
                    #cv2.waitKey(200)
            cv2.imshow('Image', copy_curr_frame)
            cv2.waitKey(10)
        cv2.destroyAllWindows()

    def clearCapture(self,capture):
        capture.release()
        cv2.destroyAllWindows()

    def calibrateShow(self):
        #pd.read_csv("tempShow.csv")
        self.detect_circles()


    def stat(self,lst):
        """Calculate mean and std deviation from the input list."""
        n = float(len(lst))
        mean = sum(lst) / n
        a =(sum(x * x for x in lst))
        stdev = sqrt(abs((sum(x * x for x in lst) / n) - (mean * mean)))
        return mean, stdev

    def splitByGroupGenerator(self,lst, n):
        cluster = []
        for i in lst:
            if len(cluster) <= 1:  # the first two values are going directly in
                cluster.append(i)
                continue

            mean, stdev = self.stat(cluster)
            if abs(mean - i) > n * stdev:  # check the "distance"
                yield cluster
                cluster[:] = []  # reset cluster to the empty list
            cluster.append(i)

        yield cluster  # yield the last cluster

    def splitByGroup(self,filterElements):
        rows = []
        print(filterElements)
        for cluster in self.splitByGroupGenerator(filterElements, 4):
            print(cluster)
            rows.append(list(cluster))
        return rows



    def orderLamps(self):
        possibleLamps = []
        #radius filter
        print("Circles : ",self.circles)
        for circle in self.circles:
                if not (circle[2] < self.min_radius or circle[2] > self.max_radius):
                    possibleLamps.append(circle)

        #sort by x axis
        possibleLamps = sorted(possibleLamps , key=lambda k: [k[0], k[1]])
        x = [xy[0] for xy in possibleLamps]

        rows = self.splitByGroup(x)
        filteredRows = []
        for row in rows:
            filteredRow = []
            for x in row:
                i = 0
                for xy in possibleLamps:
                    if xy[0] == x:
                        possibleLamps.pop(i)
                        filteredRow.append(xy)
                    i += 1
            filteredRow = sorted(filteredRow, key=lambda  k: [k[1],k[0]])
            filteredRows.append(filteredRow)

        print("Number of Rows: ",len(rows),"Rows: ",filteredRows)
        self.filteredRows = filteredRows
        self.circles = self.filteredRows
        print("Circles now Equals to ",self.circles)

    def markLampNumbers(self,frame):
        font = cv2.FONT_HERSHEY_SIMPLEX
        i = 1
        for row in self.circles:
            print(row)
            for circle in row:
                print("Marking", circle)
                cv2.putText(frame, '%d'%i, (circle[0], circle[1]), font, 2, (255, 255, 255), 2, cv2.LINE_AA)
                cv2.drawMarker(frame,(circle[0],circle[1]),(255,0,0),markerType=cv2.MARKER_CROSS,markerSize=30,thickness=2,line_type=cv2.LINE_AA)
                i+=1
        #cv2.imwrite("MarkedFrame.jpg",frame)
        return frame


    def drawCircles(self,frame):
        for row in self.circles:
            for circle in row:
                print("Drawing Circle",circle)
                cv2.circle(frame,(circle[0],circle[1]),circle[2],(0,0,255),2)
        return frame

    def rgbToXY(self,color):
        r = color[2]
        g = color[1]
        b = color[0]
        converter = Converter()
        return list(converter.rgb_to_xy(r, g, b))

    def XYToHSV(self):
        converter = Converter()
        xyArray = np.array(self.expectedXY)
        print(xyArray.shape)
        rgbArray = np.ndarray((xyArray.shape[0],xyArray.shape[1],3))
        i = 0
        for lamp in self.expectedXY:
            j = 0
            for xy in lamp:
                x = xy[0]
                y = xy[1]
                rgb = list(converter.xy_to_rgb(x,y))
                rgbArray[i][j] = [rgb[2],rgb[1],rgb[0]]
                if xy[2] == 0.0:
                    rgbArray[i][j] = [0,0,0]
                j+=1
            i+=1
        #print(self.expectedXY)
        #print(rgbArray)
        #print(rgbArray.shape)
        hsv = cv2.cvtColor(np.uint8(rgbArray), cv2.COLOR_BGR2HSV)

        return hsv


def main():
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    #a = detectColors("data/20171105_162233.mp4",False,"PAR",2)
    RGB = True;
    a = detectColors(0,True,"S30",8)
    #a = detectColors(0,True,"S38",8)
    #print(a.rgbToXY([255,200,255]))
    a.calibrateShow()

    a.print_color(200,RGB)


    if RGB:
        while True:
            _start = datetime.datetime.now()
            a.print_color(200, RGB)
            _end = datetime.datetime.now()
            _elapsed = (_end - _start).total_seconds()
            fps = 200 / _elapsed
            rgb = np.array(a.rgb)
            print(rgb)
            rgb_shape = list(rgb.shape)
            print(rgb_shape)

            hsv = cv2.cvtColor(np.uint8(rgb),cv2.COLOR_BGR2HSV)
            #print(hsv)
            #print(hsv.shape)
            cv2.waitKey()
            #for i in range(list(hsv.shape)[0]):
            fig, ax = plt.subplots(nrows=4, ncols=2)
            for i in range(8):
                h = [h_[0] for h_ in hsv[i]]
                s = [s_[0] for s_ in hsv[i]]
                v = [v_[0] for v_ in hsv[i]]
                j = np.array([k for k in range(len(hsv[i]))])
                ax[int(i/2)][i%2].plot(j, h, label='Lamp %d' % (i + 1))
                ax[int(i/2)][i%2].legend()
            print("finishd 100 frames")
            a.rgb = None
            print("FPS ", fps)
            plt.show()
            #a.testGetColor()

            text = input("prompt")


    else:
        xy = np.array(a.xy)
        print(xy)
        xy_shape = list(xy.shape)
        print(xy_shape)
        for j in range(xy_shape[0]):
            x = [x_[0] for x_ in xy[j]]
            y = [y_[1] for y_ in xy[j]]
            i = np.array([k for k in range(len(xy[j]))])
            #print(x,y,i)
            print("Standard Deviation = %d"%(j+1),xy[j].std())
            if xy[j].std() > 0.03:
                print("Flickering Lamp ",j+1)
            ax.plot(i,x ,y, label='Lamp %d'%(j+1))
        np.save('xy.npy',xy)
        ax.legend()
        fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2)
        spectrum = np.fft.fft(xy[0])
        freq = np.fft.fftfreq(len(spectrum))
        print(spectrum)
        threshold = 0.5 * max(spectrum)
        mask = abs(spectrum) > threshold
        peaks = freq[mask]
        print(peaks)
        ax1.plot(freq,spectrum)
        spectrum = np.fft.fft(xy[1])
        freq = np.fft.fftfreq(len(spectrum))
        print(freq)
        threshold = 0.5 * max(spectrum)
        mask = abs(spectrum) > threshold
        peaks = freq[mask]
        print(peaks)
        ax2.plot(freq, abs(spectrum))
        plt.show()


def main_withoutUI():
    a = detectColors(0, True, "PAR", 8)
    a.print_color(100)
    a.testGetColor()
    print("finishd 100 frames")
    xy = np.array(a.xy)
    print(xy)
    xy_shape = list(xy.shape)
    print(xy_shape)
    #a.detect_circles()


if __name__=='__main__':
    main()

#a.print_color()
#main()
#main_withoutUI()