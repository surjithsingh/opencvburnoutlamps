echo inside
OpenCVDir=/home/pi/opencvburnoutlamps
installedFlag=/home/pi/installed.txt

if [ -d $OpenCVDir ]; 
then
	echo "NoClonedlamps"
	cd opencvburnoutlamps
	git pull
	python3 Misc/discoveryScript.py
    # Control will enter here if $DIRECTORY exists.
fi
if [ ! -e $installedFlag ]
then
	echo "opencvnotinstalled"
	git clone https://surjithsingh:suji_123@bitbucket.org/surjithsingh/opencvburnoutlamps.git
	cd /home/pi/opencvburnoutlamps
	sudo apt-get install -y build-essential cmake
	sudo apt-get install -y build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev
	sudo apt-get install -y libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
	sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
	sudo apt-get install -y libxvidcore-dev libx264-dev
	sudo apt-get install -y libgtk2.0-dev libgtk-3-dev
	sudo apt-get install -y libatlas-base-dev gfortran
	sudo apt-get install -y python3-dev
	cd
	wget "https://github.com/jabelone/OpenCV-for-Pi/raw/master/latest-OpenCV.deb"
	sudo dpkg -i latest-OpenCV.deb
	rm latest-OpenCV.deb
	sudo apt-get install -y xsel xclip libxml2-dev libxslt-dev python3-lxml python3-h5py python3-numexpr python3-dateutil python3-six python3-tz python3-bs4 python3-html5lib python3-openpyxl python3-tables python3-xlrd python3-xlwt cython python3-sqlalchemy python3-xlsxwriter python3-jinja2 python3-boto python3-gflags python3-googleapi python3-httplib2 python3-zmq libspatialindex-dev python3-pymysql

	sudo pip3 install numpy  scipy scikit-learn statsmodels Twisted==16.0.0 rgbxy
	sudo apt-get install mysql-server mysql-client
	sudo apt-get install build-essential python3-dev python3-setuptools \
                     python3-numpy python3-scipy \
                     libatlas-dev libatlas3gf-base
    sudo apt-get install -y python3-pandas                
	#sudo apt-get install -y python3-numpy python3-matplotlib python3-mpltoolkits.basemap python3-scipy python3-sklearn python3-statsmodels python3-pandas python3-twisted
	touch /home/pi/installed.txt
    # Control will enter here if $DIRECTORY exists.
fi
echo End
