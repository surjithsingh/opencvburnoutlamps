
import datetime
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from colorChangeDetector import detectColors
from neuralNetwork import dataset,neuralNet
from rgbxy import Converter
from sklearn import linear_model
import pickle
import numpy as np
import pandas as pd

class Trigger(DatagramProtocol):
    def __init__(self):
        self.detectColors = detectColors(0, True, "PAR", 8)
        self.detectColors.calibrateShow()
        self.count = 0
        self.df = pd.DataFrame()
        self.converter = Converter()
        self.rmodel = pickle.load(open('regModel/Red.model','rb'))
        self.gmodel = pickle.load(open('regModel/Green.model','rb'))
        self.bmodel = pickle.load(open('regModel/Blue.model','rb'))

        data = dataset()
        print(data.trainX.shape, data.trainY.shape)
        self.net = neuralNet(data)
        self.net.evaluate()
        self.net.estimator.fit(data.trainX, data.trainY)


    def predict(self,brightness,xy,rgb):
        expected_output = self.converter.xy_to_rgb(xy[0],xy[1])
        r_data = np.array([rgb[2],brightness]).reshape(1,2)
        o_R = self.rmodel.predict(r_data)
        g_data = np.array([rgb[1], brightness]).reshape(1, 2)
        o_G = self.gmodel.predict(g_data)
        b_data = np.array([rgb[0], brightness]).reshape(1, 2)
        o_B = self.rmodel.predict(b_data)
        predicted_output = [(o_R[0][0]),o_G[0][0],o_B[0][0]]
        print(predicted_output)
        predicted_output = np.array(predicted_output)
        predicted_output[predicted_output>255] = 255
        predicted_output = predicted_output.tolist()
        xy_Predicted = self.converter.rgb_to_xy(predicted_output[0],predicted_output[1],predicted_output[2])
        predicted_output.append(list(xy_Predicted)[0])
        predicted_output.append(list(xy_Predicted)[1])
        print("Predicted ",predicted_output)
        print("Expected ", expected_output)
        print("XY - Input",xy)
        print("XY - output",self.converter.rgb_to_xy(predicted_output[0],predicted_output[1],predicted_output[2]))
        return predicted_output

    def predict_nn(self,xy,rgb):
        np_array = np.array(rgb)
        np_array /= 255
        reshaped_ip = np_array.reshape(1,3)
        prediction = self.net.estimator.predict(reshaped_ip)
        print("Predicted ",prediction, "Expected ",xy)
        return prediction

    def datagramReceived(self, data, ip):
        (host, port) = ip
        print("received %r from %s:%d" % (data, host, port))
        data = data.decode("utf-8")
        print(data)
        if "Syncing" in data:
            self.transport.write(bytearray("Synced",'utf8'), (host, port))
        if "XY" in data:
            print("XY Recieved")
            numpy_array = self.detectColors.getColor(20)
            xy = data.split()
            xy = [float(i) for i in xy[1:]]
            #predicted = self.predict(xy[2],xy,numpy_array)
            predicted = self.predict_nn(xy,numpy_array)
            #instantData = pd.DataFrame({"R_read": numpy_array[0], "G_read": numpy_array[1],"B_read": numpy_array[2], "X_sent": xy[0], "Y_sent": xy[1], "Brightness": xy[2],"Predicted_R": predicted[0], "predicted_G": predicted[1],"predicted_B":predicted[2],"predicted_X":predicted[3],"predicted_Y":predicted[4]},
            #            index=[self.count])
            instantData = pd.DataFrame(
                {"R_read": numpy_array[0], "G_read": numpy_array[1], "B_read": numpy_array[2], "X_sent": xy[0],
                 "Y_sent": xy[1], "Brightness": xy[2], "Predicted_X": predicted[0], "predicted_Y": predicted[1]},
                    index=[self.count])
            print("read",numpy_array,"sent",xy,self.count)
            print(instantData)
            self.count+=1
            self.df = self.df.append(instantData)
            self.transport.write(bytearray("GotSamples",'utf8'), (host, port))
        if "Done" in data:
            filename = "PAR600HR_withPrediction.csv"
            self.df.to_csv(filename)
reactor.listenUDP(13000, Trigger())
reactor.run()