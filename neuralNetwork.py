
import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
from sklearn.model_selection import learning_curve

class dataset:
    def __init__(self,percent=0.8):
        self.rawPD = pd.read_csv("Misc/updatedReg.csv")
        self.train = pd.DataFrame()
        self.test = pd.DataFrame()
        self.trainX = None
        self.trainY= None
        self.testX = None
        self.testY = None
        self.dataX = None
        self.dataY = None
        print("Creating a data from CSV")
        self.dataSplit(percent)
        self.getIO()
        print("Created a data from CSV")
        #self.reshape()
        self.normalize()
        print("Normalized inputs")


    def dataSplit(self,percent):
        msk = np.random.rand(self.rawPD.shape[0]) < percent
        self.train = self.rawPD[msk]
        self.test = self.rawPD[~msk]

    def getIO(self):
        camData = pd.DataFrame()
        cmdSent = pd.DataFrame()
        camData['R'] = self.train['B_read']
        camData['G'] = self.train['G_read']
        camData['B'] = self.train['R_read']
        #camData['Brightness'] = self.train['Brightness']/257
        cmdSent['X'] = self.train['X_sent']
        cmdSent['Y'] = self.train['Y_sent']
        self.trainX = camData.as_matrix()
        self.trainY = cmdSent.as_matrix()
        camData = pd.DataFrame()
        cmdSent = pd.DataFrame()
        camData['R'] = self.test['B_read']
        camData['G'] = self.test['G_read']
        camData['B'] = self.test['R_read']
        #camData['Brightness'] = self.train['Brightness'] / 257
        cmdSent['X'] = self.test['X_sent']
        cmdSent['Y'] = self.test['Y_sent']
        self.testX = camData.as_matrix()
        self.testY = cmdSent.as_matrix()
        camData = pd.DataFrame()
        cmdSent = pd.DataFrame()
        camData['R'] = self.rawPD['B_read']
        camData['G'] = self.rawPD['G_read']
        camData['B'] = self.rawPD['R_read']
        #camData['Brightness'] = self.train['Brightness'] / 257
        cmdSent['X'] = self.rawPD['X_sent']
        cmdSent['Y'] = self.rawPD['Y_sent']
        self.dataX = camData.as_matrix()
        self.dataY = cmdSent.as_matrix()

    def reshape(self):
        self.trainX = self.trainX.reshape(self.trainX.shape[0],1,3)
        self.testX = self.testX.reshape(self.testX.shape[0], 1, 3)
        self.trainY = self.trainY.reshape(self.trainY.shape[0], 1, 2)
        self.testY = self.testY.reshape(self.testY.shape[0], 1, 2)

    def normalize(self):
        self.trainX = self.trainX.astype('float32')
        self.testX = self.testX.astype('float32')
        self.trainX /= 255
        self.testX /= 255

class neuralNet():
    def __init__(self,data):
        self.estimator = None
        self.data = data
        self.seed = 7
        self.estimatorInit()


    def evaluate(self):
        kfold = KFold(n_splits=10, random_state=self.seed)
        results = cross_val_score(self.estimator, self.data.trainX, self.data.trainY, cv=kfold)
        print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))


    #define base model
    def baseline_model(self):
        # create model
        model = Sequential()
        model.add(Dense(25, input_dim=3, kernel_initializer='normal', activation='linear'))
        model.add(Dense(10, kernel_initializer='normal', activation='relu'))
        model.add(Dense(4, kernel_initializer='normal', activation='linear'))
        model.add(Dense(3, kernel_initializer='normal', activation='linear'))
        model.add(Dense(2, kernel_initializer='normal'))
        # Compile model
        model.compile(loss='mean_squared_error', optimizer='adam')
        return model


    def estimatorInit(self):
        np.random.seed(self.seed)
        self.estimator = KerasRegressor(build_fn = self.baseline_model, nb_epoch = 15000, batch_size = 10,verbose = 0)


